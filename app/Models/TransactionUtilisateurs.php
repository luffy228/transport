<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionUtilisateurs extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'user_id','agence_id','ticket_id','prix','gainSolux','num_destinataire','num_reservation','ticket_statut','num_user','nombre_ticket','moyen_id','sms','reference', 'payer'
    ];
    public function nameModel()
    {
        return 'TransactionUtilisateurs';
    }
}
