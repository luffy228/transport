<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mobilesolux extends Model
{
    //
    //use SoftDeletes;

    protected $fillable = [
        'mobilemoney','somme','moyen_id','type','pin'
    ];
    public function nameModel()
    {
        return 'Mobilesolux';
    }

}
