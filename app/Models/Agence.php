<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agence extends Model
{
    // $table->string();
    //use SoftDeletes;
    protected $fillable = [
        'name','addresse','telephone'
    ];
    public function nameModel()
    {
        return 'Agence';
    }
}
