<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgenceFrais extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'agence_id','pourcentage'
    ];
    public function nameModel()
    {
        return 'AgenceFrais';
    }
}
