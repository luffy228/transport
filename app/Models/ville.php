<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ville extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'nom'
    ];
    public function nameModel()
    {
        return 'ville';
    }
}
