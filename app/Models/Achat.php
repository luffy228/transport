<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Achat extends Model
{
    //
    protected $fillable = [
        'ticket_id','nombre_ticket','acheteur'
    ];
    public function nameModel()
    {
        return 'Achat';
    }
}
