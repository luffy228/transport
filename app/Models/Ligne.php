<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ligne extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'ville_depart_id','ville_arriver_id','name'
    ];


    public function nameModel()
    {
        return 'Ligne';
    }
}
