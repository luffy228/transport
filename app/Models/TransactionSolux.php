<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionSolux extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'agence_id','nombreTicket','prix','numeroAgence','statut' , 'reference', 'operateurs','code','key'
    ];
    public function nameModel()
    {
        return 'TransactionSolux';
    }
}
