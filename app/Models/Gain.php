<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gain extends Model
{
    //
   // use SoftDeletes;
    protected $fillable = [
        'agence_id','ticket_id','profit'
    ];
    public function nameModel()
    {
        return 'Gain';
    }
}
