<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model
{
    //

    protected $fillable = [
        'user_id','agence_id','ticket_id','debut','duree','fin'
    ];
    public function nameModel()
    {
        return 'Abonnement';
    }
}
