<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgenceLigne extends Model
{
    //
   // use SoftDeletes;
    protected $fillable = [
        'agence_id','ligne_id'
    ];


    public function nameModel()
    {
        return 'AgenceLigne';
    }
}
