<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DestinataireInfo extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'num_destinataire','name'
    ];
    public function nameModel()
    {
        return 'DestinataireInfo';
    }
}
