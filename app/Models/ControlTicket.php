<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ControlTicket extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'user_id','transaction_id'
    ];
    public function nameModel()
    {
        return 'ControlTicket';
    }


}
