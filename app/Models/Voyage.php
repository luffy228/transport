<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voyage extends Model
{

    protected $fillable = [
        'heureDepart','heureArriver','place'
    ];
    public function ticketMother()
    {
        return $this->morphOne('App\Models\Tickets','ticketsType');
    }

    public function nameModel()
    {
        return 'Voyage';
    }
}
