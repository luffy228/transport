<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgenceMobilemoney extends Model
{
    //
    //use SoftDeletes;

    protected $fillable = [
        'agence_id','mobilemoney','moyen_id'
    ];
    public function nameModel()
    {
        return 'AgenceMobilemoney';
    }
}
