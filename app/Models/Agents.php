<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agents extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'agence_id'
    ];
    public function userMother()
    {
        return $this->morphOne('App\Models\User','userType');
    }
    public function nameModel()
    {
        return 'Agents';
    }
}
