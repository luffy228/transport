<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tickets extends Model
{

    //use SoftDeletes;
    protected $fillable = [
        'agence_ligne_id','heureDepart','heureArriver','prix','frais','totalPlace','place','poids','dateDepart','dateArriver','duree'];
    public function nameModel()
    {
        return 'Tickets';
    }
}
