<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MoyenPayement extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'operateurs','ussd','key'
    ];
    public function nameModel()
    {
        return 'MoyenPayement';
    }
}
