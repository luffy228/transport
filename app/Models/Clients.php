<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clients extends Model
{
    //use SoftDeletes;
    protected $fillable = [
        'piece'
    ];
    public function userMother()
    {
        return $this->morphOne('App\Models\User','userType');
    }

    public function nameModel()
    {
        return 'Clients';
    }
}
