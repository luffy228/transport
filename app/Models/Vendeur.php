<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendeur extends Model
{
    //
    //use SoftDeletes;
    protected $fillable = [
        'carteIdentite','cfe'
    ];
    public function userMother()
    {
        return $this->morphOne('App\Models\User','userType');
    }
}
