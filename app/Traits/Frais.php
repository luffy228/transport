<?php

namespace App\Traits;
trait Frais
{

    protected function fraisSms()
    {
        return 20;
    }

    protected function fraisSocket()
    {
        return 20;
    }

    protected function fraisOperateur($prix)
    {
        if ($prix >= 0 && $prix <= 5000) {
            return 100;
        }

        if ($prix >= 5001 && $prix <= 15000) {
            return 200;
        }

        if ($prix >= 15001 && $prix <= 50000) {
            return 300;
        }

        if ($prix >= 50001 && $prix <= 100000) {
            return 400;
        }

    }



}
