<?php

use Illuminate\Support\Facades\Route;

function set_active( $route ) {
    return 
    (strpos(Route::currentRouteName(),$route)==0)?'active':'';
    
}
