<?php

namespace App\Jobs;

use App\Models\TransactionSolux;
use App\Repositories\Implementation\TransactionSoluxRepository;
use App\Traits\ApiResponser;
use App\Traits\ExternalApi;
use Ably\AblyRest;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Type\Integer;

class TransactionUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout= 300;
    use ApiResponser;
    protected $transaction;
    protected $transactionSolux;
    protected $smsinfo;
    protected $numUser;
    protected $numDest;
    protected $agenceId;
    use ExternalApi;




    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $information, array $infoSMS, string $numUser, string $numDest,int $agenceId)
    {
        $this->transaction = $information;
        $this->smsinfo = $infoSMS;
        $this->numUser = $numUser;
        $this->numDest = $numDest;
        $this->agenceId = $agenceId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $echec = TransactionSolux::where('statut','Echec')->get();


        try {


            $ably = new AblyRest($this->ablyToken());
            $ably->time();
            if (count($echec) == 0) {
                sleep(10);
                $ably->channel($this->ablyChannel())->publish('',$this->transaction);

            }
            if (count($echec) > 0) {
                // Lancer la requete changer le statut des echecs en Valider avant de lancer la transaction en cours
                foreach ($echec as $transactionEchouer) {
                    $infooperateur = DB::table('agence_mobilemoneys')
                                        ->where('agence_mobilemoneys.mobilemoney',$transactionEchouer["numeroAgence"])
                                        ->join('mobilesoluxes','mobilesoluxes.moyen_id','=','agence_mobilemoneys.moyen_id')
                                        ->join('moyen_payements','moyen_payements.id','=','agence_mobilemoneys.moyen_id')
                                        ->select('moyen_payements.operateurs','moyen_payements.ussd','moyen_payements.key','mobilesoluxes.pin')
                                        ->first();

                    $socket =[
                        'operateurs'=>$infooperateur->operateurs,
                        'numero'=>$transactionEchouer["numeroAgence"],
                        'somme'=>(string)$transactionEchouer["prix"],
                        'key'=>$infooperateur->key,
                        'code'=>$infooperateur->ussd,
                        'pin'=>$infooperateur->pin,
                        'nombreTicket'=>$transactionEchouer["nombreTicket"],
                    ];
                    sleep(10);

                    $ably->channel($this->ablyChannel())->publish('',$socket);
                    $updatetransaction = DB::table('transaction_soluxes')
                                                ->where('id',$transactionEchouer["id"])
                                                ->update(array('statut'=>"Valider"));


                }
                sleep(10);
                $ably->channel($this->ablyChannel())->publish('',$this->transaction);
            }

            $this->envoieSMS($this->smsinfo,$this->numUser,$this->numDest);

        } catch (Exception $e) {
                $transaction = new TransactionSolux();
                $transaction->agence_id =$this->agenceId;
                $transaction->nombreTicket =$this->smsinfo['place'];
                $transaction->prix =$this->transaction['somme'];
                $transaction->numeroAgence =$this->transaction['numero'];
                $transaction->statut ="Echec";
                $transaction->save();
                Cache::lock('transaction')->forceRelease();
        }
    }
    public function envoieSMS($info,$num_user,$num_dest)
    {
        if ($num_user!= $num_dest) {
            $customer_id = $this->customerIdTelesign();
            $token =$this->telesignToken();
            $send_from = $this->telesignFrom();
            $to = "22870517794";
            $message = trim("Transfert effectuer avec succes ", "\r").
                       trim(" Agence :  ".$info['agence'], "\r").
                       trim(" Ligne : ".$info['ligne'], "\r").
                       trim(" Depart : ".$info['depart'], "\r").
                       trim(" Duree : ".$info['duree'], "\r").
                       trim(" Nombre de place : ".$info['place'], "\r").
                       trim(" Numero de reservation : ".$info['numeroReservation'], "\r").
                       " Merci pour la confiance , Bon trajet";

            $client = new Client(['verify' => false ]);
            $response = $client->request('POST', 'https://rest-api.telesign.com/v1/messaging',[
                        'form_params' => [
                            'phone_number' => $to,
                            'message'=>$message,
                            'message_type'=>"ARN"
                        ],
                        'auth' => [
                            $customer_id,
                            $token
                        ]
                    ]);

        }
    }
}
