<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AgenceRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Agence';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function findname($name)
    {
        $record = $this->model->where('name',$name)->first();
        return $record;
    }

    public function listeTicket($agence_id)
    {
        $record = DB::table('agences')
                    ->where('agences.id',$agence_id)
                    ->join('agence_lignes','agence_lignes.agence_id','=','agences.id')
                    ->join('lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->select('tickets.id','lignes.name','tickets.heureDepart','tickets.heureArriver','tickets.dateDepart','tickets.dateArriver','tickets.duree','tickets.prix','tickets.totalPlace','tickets.place','tickets.poids')
                    ->get();
        return $record;
    }

    public function findResponsable($agence_id)
    {
        $record = DB::table('agents')
                    ->where('agents.agence_id',$agence_id)
                    ->join('agences','agences.id','=','agents.agence_id')
                    ->join('users','users.user_id','=','agents.id')
                    ->join('model_has_roles','model_has_roles.model_id','=','users.id')
                    ->where('model_has_roles.role_id',2)
                    ->select('agences.id as agence_id','agences.name as agence','agences.addresse as adresse','agences.telephone as numero','users.name as responsable')
                    ->first();
        return $record;
    }

    public function getAllController($agence_id)
    {
        $record = DB::table('agents')
                    ->where('agents.agence_id',$agence_id)
                    ->join('agences','agences.id','=','agents.agence_id')
                    ->join('users','users.user_id','=','agents.id')
                    ->join('model_has_roles','model_has_roles.model_id','=','users.id')
                    ->where('model_has_roles.role_id',3)
                    ->select('users.name','users.telephone')
                    ->get();
        return $record;
    }

    public function getAllAgents($agence_id)
    {
        $record = DB::table('agents')
                    ->where('agents.agence_id',$agence_id)
                    ->join('agences','agences.id','=','agents.agence_id')
                    ->join('users','users.user_id','=','agents.id')
                    ->join('model_has_roles','model_has_roles.model_id','=','users.id')
                    ->where('model_has_roles.role_id',6)
                    ->select('users.name','users.telephone')
                    ->get();
        return $record;
    }

    public function gainAgence()
    {
        $record = DB::table('agences')
                    ->join('agence_frais','agence_frais.agence_id','=','agences.id')
                    ->select('agences.name as agence','agence_frais.pourcentage')
                    ->get();
        return $record;
    }


}
