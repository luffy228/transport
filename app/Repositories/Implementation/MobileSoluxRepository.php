<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MobileSoluxRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Mobilesolux';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function findNumero($numero)
    {
        $record = $this->model->where('mobilemoney',$numero)->first();
        return $record;
    }

    public function information()
    {
        $record = $this->model
                        ->join('moyen_payements','mobilesoluxes.moyen_id','=','moyen_payements.id')
                        ->select('mobilesoluxes.mobilemoney','mobilesoluxes.somme','moyen_payements.operateurs')
                        ->get();
        return $record;
    }

    public function getNumero($moyen_id,$somme)
    {
        $record = $this->model
                        ->where('moyen_id',$moyen_id)
                        ->where('type','E')
                        ->where('somme','>=',$somme)
                        ->get();
        return $record;
    }

    public function getNumerobyType($type)
    {
        $record = DB::table('mobilesoluxes')
                        ->where('type',$type)
                        ->first();
        return $record;
    }

    public function getNumerobyTypeMoyen($type,$moyen_id)
    {
        $record = DB::table('mobilesoluxes')
                        ->where('type',$type)
                        ->where('moyen_id',$moyen_id)
                        ->first();
        return $record;
    }

    public function updateModel(array $data, $id)
    {
        DB::table('mobilesoluxes')->where('id',$id)->update($data);
    }


}
