<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\Validator;

class AgenceLigneRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\AgenceLigne';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function findLigne($agence_id , $ligne_id)
    {
        $record = $this->model->where('agence_id',$agence_id)
                                ->where('ligne_id',$ligne_id)
                                ->first();
        return $record;
    }




}
