<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ControlTicketRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\ControlTicket';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function ticketControl($user,$debut,$fin)
    {
        $record = DB::table('control_tickets')
                        ->where('control_tickets.user_id',$user)
                        ->join('transaction_utilisateurs','transaction_utilisateurs.id','=','control_tickets.transaction_id')
                        ->whereBetween('control_tickets.created_at',[$debut,$fin])
                        ->get();
        return $record;

    }




}
