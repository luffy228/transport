<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\User';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function information($userId)
    {
        $record = $this->model
                        ->where('id',$userId)
                        ->first();
        return $record;
    }

    public function findControllerInformation($user)
    {
        // Trouver le user_id normalement
        // Correspondre ce user avec agents pour obtenir l'id de l'agence
        $record = DB::table('users')
                    ->where('users.id',$user)
                    ->join('agents','agents.id','=','users.user_id')
                    ->join('agences','agences.id','=','agents.agence_id')
                    ->select('users.name','users.username','users.email','users.telephone','agences.name as agence')
                    ->first();
        return $record;

    }

    public function findNumero($num)
    {
        $record = $this->model
                        ->where('telephone',$num)
                        ->first();
        return $record;
    }

    public function findAgence($user)
    {
        // Trouver le user_id normalement
        // Correspondre ce user avec agents pour obtenir l'id de l'agence
        $record = DB::table('users')
                    ->where('users.id',$user)
                    ->join('agents','agents.id','=','users.user_id')
                    ->select('agents.agence_id')
                    ->first();
        return $record;

    }






}
