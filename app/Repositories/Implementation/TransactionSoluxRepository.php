<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TransactionSoluxRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\TransactionSolux';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }
    /*
    public function findWaitingTransaction($num_user)
    {
        $record = $this->model
                        ->where('transaction_statut',"Attente")
                        ->where('num_user',$num_user)
                        ->first();
        return $record;
    }

    */

    public function countAgenceTransfert($agence_id,$debut,$fin)
    {
        $record = $this->model
                        ->where('agence_id',$agence_id)
                        ->whereBetween('created_at',[$debut,$fin])
                        ->count();

        return $record;
    }

    public function countAgenceGain($agence_id,$debut,$fin)
    {
        $record = $this->model
                        ->where('agence_id',$agence_id)
                        ->whereBetween('created_at',[$debut,$fin])
                        ->sum('prix');



        return $record;
    }

    public function transfertHistory($debut,$fin)
    {
        $record = DB::table('transaction_soluxes')
                    ->whereBetween('created_at',[$debut,$fin])
                    ->count();
        return $record;
    }


    public function InformationtransfertHistory($debut,$fin)
    {
        $record = DB::table('transaction_soluxes')
                    ->whereBetween('transaction_soluxes.created_at',[$debut,$fin])
                    ->join('agences','agences.id','=','transaction_soluxes.agence_id')
                    ->select('agences.name as agence','transaction_soluxes.nombreTicket','transaction_soluxes.prix','transaction_soluxes.numeroAgence','transaction_soluxes.created_at')
                    ->get();
        return $record;
    }

    public function AgenceInformationtransfertHistory($agence_id,$debut,$fin)
    {
        $record = DB::table('transaction_soluxes')
                    ->whereBetween('transaction_soluxes.created_at',[$debut,$fin])
                    ->where('transaction_soluxes.agence_id',$agence_id)
                    ->select('transaction_soluxes.nombreTicket','transaction_soluxes.prix','transaction_soluxes.numeroAgence','transaction_soluxes.created_at')
                    ->get();
        return $record;
    }

    public function moneySendSoluxHistory($debut,$fin)
    {
        $record = DB::table('transaction_soluxes')
                    ->whereBetween('created_at',[$debut,$fin])
                    ->sum('prix');
        return $record;
    }


}
