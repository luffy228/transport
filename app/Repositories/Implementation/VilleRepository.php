<?php

namespace  App\Repositories\Implementation;

use App\Models\ville;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class VilleRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
        'ville'=>'required|string|Min:3',
    ];


    public function model()
    {
        return 'App\Models\ville';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        return $valider;
    }

    public function findname($name)
    {
        $record = DB::table('villes')
                    ->where('nom',$name)
                    ->first();
        return $record;
    }




}
