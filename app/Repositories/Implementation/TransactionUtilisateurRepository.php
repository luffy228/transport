<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TransactionUtilisateurRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\TransactionUtilisateurs';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function findWaitingTransaction($num_user)
    {
        $record = $this->model
                        ->where('transaction_statut',"Attente")
                        ->where('num_user',$num_user)
                        ->first();
        return $record;
    }

    public function findNumeroReservation($reservation,$agence_id)
    {
        $record = $this->model
                        ->where('num_reservation',$reservation)
                        ->where('agence_id',$agence_id)
                        ->first();
        return $record;
    }

    public function findReference($reservation,$agence_id)
    {
        $record = $this->model
                        ->where('reference',$reservation)
                        ->where('agence_id',$agence_id)
                        ->first();
        return $record;
    }

    public function countSale($numvendeur,$debut,$fin)
    {
        $record = $this->model
                        ->where('num_user',$numvendeur)
                        ->whereBetween('created_at',[$debut,$fin])
                        ->get();
        return $record;
    }

    public function userReservation($numUser)
    {
        $record = $this->model
                        ->where('num_destinataire',$numUser)
                        ->orderBy('created_at', 'desc')
                        ->get();
        return $record;

    }

    public function gainSoluxHistory($debut,$fin)
    {
        $record = DB::table('transaction_utilisateurs')
                    ->whereBetween('created_at',[$debut,$fin])
                    ->sum('gainSolux');
        return $record;
    }

    public function moneyReceiveHistory($debut,$fin)
    {
        $record = DB::table('transaction_utilisateurs')
                    ->whereBetween('created_at',[$debut,$fin])
                    ->sum('prix');
        return $record;
    }

    public function transfertHistory($debut,$fin)
    {
        $record = DB::table('transaction_utilisateurs')
                    ->whereBetween('created_at',[$debut,$fin])
                    ->count();
        return $record;
    }

    public function transfertHistorySms($debut,$fin)
    {
        $record = DB::table('transaction_utilisateurs')
                    ->whereBetween('created_at',[$debut,$fin])
                    ->where('sms',"oui")
                    ->count();
        return $record;
    }

    public function informationtransfertHistory($debut,$fin)
    {
        $record = DB::table('transaction_utilisateurs')
                    ->whereBetween('created_at',[$debut,$fin])
                    ->get();
        return $record;
    }





}
