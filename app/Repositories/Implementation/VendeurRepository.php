<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VendeurRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Vendeur';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function getAllVendeurs()
    {
        $record = DB::table('vendeurs')
                    ->join('users','users.user_id','=','vendeurs.id')
                    ->join('model_has_roles','model_has_roles.model_id','=','users.id')
                    ->where('model_has_roles.role_id',5)
                    ->select('users.name','users.telephone')
                    ->get();
        return $record;
    }


}
