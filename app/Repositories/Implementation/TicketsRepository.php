<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TicketsRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\Tickets';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function information($ticket_id)
    {
        $record = DB::table('tickets')
                    ->join('agence_lignes','agence_lignes.id','=','tickets.agence_ligne_id')
                    ->join('agences','agence_lignes.agence_id','=','agences.id')
                    ->join('lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->where('tickets.id','=',$ticket_id)
                    ->select('tickets.id','agence_lignes.agence_id as agence_id','tickets.prix as prix','agences.name as agence','lignes.name as ligne','tickets.dateDepart','tickets.heureDepart','tickets.dateArriver','tickets.heureArriver','tickets.place','tickets.totalPlace','tickets.frais','tickets.duree')
                    ->first();
            return $record;

    }

    public function informationLigne($ticket_id)
    {
        $record = DB::table('tickets')
                    ->join('agence_lignes','agence_lignes.id','=','tickets.agence_ligne_id')
                    ->join('lignes','lignes.id','=','agence_lignes.ligne_id')
                    ->where('tickets.id','=',$ticket_id)
                    ->select('tickets.id','lignes.name as ligne','tickets.dateDepart as depart','tickets.dateArriver as dateArriver','tickets.heureArriver as heureArriver','tickets.heureDepart as heure','tickets.duree')
                    ->first();
            return $record;

    }

    public function findTicket($villeDepart,$villeArriver,$dateDepart,$place,$poids)
    {
        $name = $villeDepart."-".$villeArriver;

        if ($place == null && $poids == null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();
            return $record;
        }

        if ($poids == null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.place','>=',$place)
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();

            return $record;

        }

        if ($place == null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.poids','>=',$poids)
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();
            return $record;

        }

        if ($place != null && $poids != null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.place','>=',$place)
                    ->where('tickets.poids','>=',$poids)
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();
            return $record;

        }


    }

    public function findTicketAgence($villeDepart,$villeArriver,$dateDepart,$place,$poids,$agenceid)
    {
        $name = $villeDepart."-".$villeArriver;

        if ($place == null && $poids == null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->where('agences.id',$agenceid)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();
            return $record;
        }

        if ($poids == null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.place','>=',$place)
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->where('agences.id',$agenceid)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();

            return $record;

        }

        if ($place == null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.poids','>=',$poids)
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->where('agences.id',$agenceid)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();
            return $record;

        }

        if ($place != null && $poids != null) {

            $record = DB::table('lignes')
                    ->where('lignes.name',$name)
                    ->join('agence_lignes','agence_lignes.ligne_id','=','lignes.id')
                    ->join('tickets','tickets.agence_ligne_id','=','agence_lignes.id')
                    ->join('agences','agences.id','agence_lignes.agence_id')
                    ->where('tickets.place','>=',$place)
                    ->where('tickets.poids','>=',$poids)
                    ->where('tickets.dateDepart','=',$dateDepart)
                    ->where('agences.id',$agenceid)
                    ->select('tickets.id','lignes.name as ligne','agences.name as agence','tickets.prix as prix','tickets.place as place','tickets.heureDepart','tickets.heureArriver','tickets.poids','tickets.frais','tickets.duree')
                    ->get();
            return $record;

        }


    }


}
