<?php

namespace  App\Repositories\Implementation;

use App\Models\User;
use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class RoleRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
        'role'=>'required|unique:Spatie\Permission\Models\Role,name|string',
    ];
    public function revokePermissionTo(Role $role, string $permissions) {
        $role->revokePermissionTo($permissions);
    }

    public function givePermissionTo(Role $role, array $permissions) {
        $role->givePermissionTo($permissions);
    }

    public function hasRole(User $user, $roles = []) {
        return $user->hasRole($roles);
    }

    public function hasAnyRole(User $user, $roles = []) {
        return $user->hasAnyRole($roles);
    }

    public function hasAllRoles(User $user, $roles = []) {
        return $user->hasAllRoles($roles);
    }

    public function assignRole(User $user, array $roles) {
        return $user->assignRole($roles);
    }


    public function all()
    {
        return $this->model->all();
    }

    public function model()
    {
        return 'Spatie\Permission\Models\Role';
    }

    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }



}
