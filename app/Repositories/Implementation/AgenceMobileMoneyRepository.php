<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AgenceMobileMoneyRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\AgenceMobilemoney';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function getNumerobyagence($agence_id)
    {
        $record = $this->model->where('agence_id',$agence_id)->get();
        return $record;
    }

    public function findNumero($numero)
    {
        $record = $this->model->where('mobilemoney',$numero)->first();
        return $record;
    }

    public function informationMobileMoney($agence_id)
    {
        $record = DB::table('agence_mobilemoneys')
                        ->where('agence_id',$agence_id)
                        ->join('moyen_payements','moyen_payements.id','=','agence_mobilemoneys.moyen_id')
                        ->first();
        return $record;
    }








}
