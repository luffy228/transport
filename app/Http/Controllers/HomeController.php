<?php

namespace App\Http\Controllers;


use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function login(Request $request)
    {
        $request->validate([
            "name" => 'required|string',
            "password" => 'required|string',
        ]);
        if(Auth::attempt(['username' => request('name'), 'password' => request('password')]))
        {
            $role = Auth::user()->getRoleNames()[0];
            if ($role == "SuperAdministrateur" ||$role == "AdministrateurAgence" ||$role == "Agents") {
                Alert::success('Bienvenue sur E-billet');
                return redirect()->route('accueil');
            }
            Alert::error("Vous n'avez pas les droits pour vous connecter");
            return redirect()->back();
        }
        else {
            Alert::error("Username ou mot de passe errone");
            return redirect()->back();
        }

        // AJouter sweet Alert
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('loginPage');
    }

}
