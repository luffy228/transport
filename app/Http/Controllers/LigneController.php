<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\AgenceLigneRepository;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\LigneRepository;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\VilleRepository;
use App\Traits\Frais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LigneController extends Controller
{

    protected $agenceRepo;
    protected $ticketRepo;
    protected $ligneRepo;
    protected $agenceligneRepo;
    protected $villeRepo;
    use Frais;


    function __construct(App $app)
    {

        $this->ligneRepo = new LigneRepository($app);
        $this->villeRepo = new VilleRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
        $this->villeRepo = new VilleRepository($app);
        $this->agenceligneRepo = new AgenceLigneRepository($app);
        $this->ticketRepo = new TicketsRepository($app);
    }
    //

    public function addLigne(Request $request)
    {

        $this->ligneRepo->validateData();
        $firstVille = $this->villeRepo->findname($request["villedepart"]);
        $secondVille =  $this->villeRepo->findname($request["villearriver"]);

        $ligne_request = [
            'ville_depart_id'=>$firstVille->id,
            'ville_arriver_id'=>$secondVille->id,
            'name'=>$request["villedepart"]."-".$request["villearriver"]
        ];
        $this->ligneRepo->create($ligne_request);
        printf("Ticket voyage creer");

    }
    /**
     * fonction qui permet d'affecter une ligne a une agence et de creer le ticket
     */

    public function affecterLigne(Request $request)
    {

        // Creer un ticket
        $this->agenceligneRepo->validateData();
        $frais = $this->fraisSms()+$this->fraisSocket()+$this->fraisOperateur($request["prix"]);
        $agence = $this->agenceRepo->findname($request["agence"]);
        $ligne =  $this->ligneRepo->findname($request["ligne"]);
        $agenceLigne_request = [
            'agence_id'=>$agence->id,
            'ligne_id'=>$ligne->id,
        ];
        $agenceLigne = $this->agenceligneRepo->create($agenceLigne_request);
        $this->ticketRepo->validateData();
        $ticket_request = [
            'agence_ligne_id'=>$agenceLigne["id"],
            'heureDepart'=>$request["heuredepart"],
            'heureArriver'=>$request["heurearriver"],
            'dateDepart'=>$request["datedepart"],
            'dateArriver'=>$request["datearriver"],
            'prix'=> $request["prix"],
            'frais'=>$frais,
            'place'=> $request["place"],
            'poids'=> $request["poids"],
        ];

        $this->ticketRepo->create($ticket_request);

        printf("Ticket voyage creer");

    }
}
