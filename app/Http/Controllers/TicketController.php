<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\LigneRepository;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\VoyageRepository;
use App\Traits\ApiResponser;
use Facade\FlareClient\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TicketController extends Controller
{
    //

    protected $agenceRepo;
    protected $ticketRepo;
    protected $voyageRepo;
    protected $ligneRepo;
    use ApiResponser;


    function __construct(App $app)
    {
        $this->agenceRepo = new AgenceRepository($app);

        $this->ticketRepo = new TicketsRepository($app);

        $this->voyageRepo = new VoyageRepository($app);

        $this->ligneRepo = new LigneRepository($app);

        $this->agenceRepo = new AgenceRepository($app);


    }

    public function updatePrice(Request $request)
    {
        $this->ticketRepo->validateData();
        $ticket_id = $request["ticket_id"];
        $ticket_request = [
            'prix'=> $request["prix"],
        ];
        $this->ticketRepo->update($ticket_request,$ticket_id);

    }

    public function updateTicket(Request $request)
    {
        $this->ticketRepo->validateData();
        $ticket_id = $request["ticket_id"];
        $ticket_request = [
            'heureDepart'=>$request["heuredepart"],
            'heureArriver'=>$request["heurearriver"],
            'dateDepart'=>$request["datedepart"],
            'dateArriver'=>$request["datearriver"],
            'place'=> $request["place"],
            'poids'=> $request["poids"],
        ];
        $this->ticketRepo->update($ticket_request,$ticket_id);

    }

    public function find(Request $request)
    {
        $villedepart = $request["villedepart"];
        $villearriver = $request["villearriver"];
        $dateDepart=$request["datedepart"];
        $place = $request["place"];
        $poids = $request["poids"];
        $find =   $this->ticketRepo->findTicket($villedepart,$villearriver,$dateDepart,$place,$poids);
        return $this->successResponse($find);
    }


}
