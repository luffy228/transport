<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\ClientsRepository;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use App\Repositories\Implementation\UserRepository;
use App\Repositories\Implementation\VilleRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $userRepo;
    protected $clientRepo;
    protected $transactionRepo;
    protected $ticketRepo;
    protected $agenceRepo;
    protected $villeRepo;

    use ApiResponser;
    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->clientRepo = new ClientsRepository($app);
        $this->transactionRepo = new TransactionUtilisateurRepository($app);
        $this->ticketRepo = new TicketsRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
        $this->villeRepo = new VilleRepository($app);

    }

    /**
     * @OA\Post(
     * path="/api/v1/login",
     * description="adding an login",
     * operationId="loginVoyageur",
     * tags={"Voyageur"},
     *  @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{token:'',role:Voyageur,information:{id: 3,name:barry,username:barry,email:barry@gmail.com,telephone:70517794,user_type:,user_id:}"),
     *     )
     *  ),
     * )
     */

    public function login()
    {
        if (Auth::attempt(['username' => request('name'), 'password' => request('password')]) || Auth::attempt(['email' => request('name'), 'password' => request('password')])) {
            $user = Auth::user();
            $role = $user->getRoleNames();

            if ($role[0] == "Voyageur") {
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['role'] = "Voyageur";
                $data['information'] = $this->userRepo->Information($user->id);
                return $this->successResponse($data);
            }

            if ($role[0] == "Vendeur") {
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['role'] = "Vendeur";
                $data['information'] = $this->userRepo->Information($user->id);
                return $this->successResponse($data);
            }

            if ($role[0] == "Controleur") {
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['role'] = "Controleur";
                $data['information'] = $this->userRepo->findControllerInformation($user->id);
                return $this->successResponse($data);
            }

            if ($role[0] != "Voyageur" && $role[0]!= "Vendeur" && $role[0]!= "Controleur") {
                return $this->errorResponse('Vous n avez pas le droit necessaire', 402);
            }
        } else {
            return $this->errorResponse('Authentification failled: email or password incorrect', 402);
        }
    }

    /**
     * @OA\Post(
     * path="/api/v1/register",
     * description="adding an register",
     * operationId="RegisterVoyageur",
     * tags={"Voyageur"},
     *  @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *
     * @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="username",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * * @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * * @OA\Parameter(
     *      name="telephone",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * * @OA\Parameter(
     *      name="piece",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{token:'',role:Voyageur,information:{id: 3,name:barry,username:barry,email:barry@gmail.com,telephone:70517794,user_type:,user_id:}"),
     *     )
     *  ),
     * )
     */

    public function register(Request $request)
    {
        $this->clientRepo->validateData();

        $client_request = [
            'piece' => $request["piece"],
        ];
        $client = $this->clientRepo->create($client_request);

        $this->userRepo->validateData();
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->clientRepo->model(),
            'user_id' => $client['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(4);
        $user->assignRole($role1);
        $data['token'] =  $user->createToken('token')->accessToken;
        $data['role'] = "Voyageur";
        $data['information'] = $this->userRepo->Information($user->id);
        return $this->successResponse($data);

    }

    /**
     * @OA\Post(
     * path="/api/v1/user/reservation",
     * description="get all reservation for an user",
     * operationId="UserReservation",
     * tags={"Voyageur"},
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{agence:Nagode,ligne:Lome-Notse,depart:2021-07-06 00:18:30,place:1,statut:Valider,numeroreservation:5142054,duree:00:50:00}"),     *     )
     *  ),
     * )
     */
    public function ListeReservation()
    {
        $result = $this->transactionRepo->userReservation(auth()->guard('api')->user()->telephone);
        $data = [];
        if ($result != null) {
            foreach ($result as $transaction) {
                $agence = $this->agenceRepo->show($transaction["agence_id"]);
                $ticket = $this->ticketRepo->informationLigne($transaction["ticket_id"]);
                $info = [
                    'reference'=>$transaction["reference"],
                    'agence'=>$agence["name"],
                    'ligne'=>$ticket->ligne,
                    'dateDepart'=>$ticket->depart." a ".$ticket->heure,
                    'dateArriver'=>$ticket->dateArriver." a ".$ticket->heureArriver,
                    'duree'=>$ticket->duree,
                    'prix'=>$transaction["prix"],
                    'numero'=>$transaction["num_destinataire"],
                    'statut'=>$transaction["ticket_statut"],
                ];
                array_push($data,$info);
            }

            return $this->successResponse($data);

        }
        return $this->successResponse($result);
    }


     public function updatePassword(Request $request)
    {

            $form_request = [
                'password' => bcrypt($request["password"]),
            ];
            $updateUser = $this->userRepo->update($form_request,auth()->guard('api')->user()->id);

            return $this->successResponse($updateUser);


    }

    /**
     * @OA\Post(
     * path="/api/v1/logout",
     * description="logout",
     * operationId="UserLogout",
     * tags={"Logout"},
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example=""),     *     )
     *  ),
     * )
     */

    public function forgetPassword(Request $request)
    {
        // Quand il va taper le nouveau mot de passe le loger en meme temps.

        $user = User::where('telephone', $request["telephone"])->first();

        $form_request = [
            'password' => bcrypt($request["password"]),
        ];
        $updateUser = $this->userRepo->update($form_request,$user["id"]);
        $data['token'] =  $user->createToken('token')->accessToken;
        $data['role'] = "Voyageur";
        $data['information'] = $this->userRepo->Information($user->id);
        return $this->successResponse($data);

    }

    public function logout(Request $request)
    {
        $token = Auth::guard('api')->user()->token();
        $token->revoke();
        return $this->successResponse('Deconnexion reussi!');
    }

    public function getVille()
    {
        $ville = $this->villeRepo->all();
        return $this->successResponse($ville);
    }
}
