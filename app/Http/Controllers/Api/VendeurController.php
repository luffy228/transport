<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class VendeurController extends Controller
{
    //
    protected $userRepo;
    protected $transactionRepo;
    protected $ticketRepo;

    use ApiResponser;
    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->ticketRepo = new TicketsRepository($app);
        $this->transactionRepo = new TransactionUtilisateurRepository($app);

    }

    /**
     * @OA\Post(
     * path="/api/v1/vendeur/login",
     * description="adding an login",
     * operationId="loginVendeur",
     * tags={"Vendeur"},
     *  @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{token:'',role:Voyageur,information:{id: 3,name:barry,username:barry,email:barry@gmail.com,telephone:70517794,user_type:,user_id:}"),
     *     )
     *  ),
     * )
     */

    public function login()
    {
        if (Auth::attempt(['username' => request('name'), 'password' => request('password')])) {
            $user = Auth::user();
            $role = $user->getRoleNames();

            if ($role[0] == "Vendeur") {
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['role'] = "Vendeur";
                $data['information'] = $this->userRepo->Information($user->id);
                return $this->successResponse($data);
            }

            if ($role[0] != "Vendeur") {
                return $this->errorResponse('Vous n avez pas le droit necessaire', 402);
            }
        } else {
            return $this->errorResponse('Authentification failled: email or password incorrect', 403);
        }
    }

    /**
     * @OA\Post(
     * path="/api/v1/vendeur/ticketSales",
     * description="count ticket sale for today",
     * operationId="VendeurSalesToday",
     * tags={"Vendeur"},
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{Ticket vendus: 2,Somme recuperer:8000}"),
     *     )
     *  ),
     * )
     */

    public function salesToday(Request $request)
    {
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");
        $result = $this->transactionRepo->countSale(auth()->guard('api')->user()->telephone,$today,$fin);
        return $this->SalesResult($result);
    }

    /**
     * @OA\Post(
     * path="/api/v1/vendeur/historyticketSales",
     * description="count history ticket sales",
     * operationId="VendeurSales",
     * tags={"Vendeur"},
     *  @OA\Parameter(
     *      name="numVendeur",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="debut",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="fin",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{Ticket vendus: 2,Somme recuperer:8000}"),
     *     )
     *  ),
     * )
     */

    public function historySales(Request $request)
    {
        $datedebut = strtotime($request['debut']);
        $debut = date("Y-m-d H:i:s",$datedebut);
        $datefin = strtotime($request['fin']);
        $fin = date("Y-m-d H:i:s",$datefin);
        $result = $this->transactionRepo->countSale(auth()->guard('api')->user()->telephone,$debut,$fin);
        return $this->SalesResult($result);


    }

    public function SalesResult($result)
    {
        $somme = 0;
        $listInfo = [];
        if ($result != null) {
            foreach ($result as $sales) {
                $somme+= $this->pourcentageVendeur( $sales["prix"]) ;
                $ticket = $this->ticketRepo->information($sales["ticket_id"]);
                $info = [
                    'agence'=>$ticket->agence,
                    'prix'=>$ticket->prix,
                    'numero'=>$sales["num_destinataire"],
                    'reference'=>$sales["reference"],
                    'statut'=>$sales["ticket_statut"],
                    'duree'=>$ticket->duree,
                    'ligne'=>$ticket->ligne,
                    'depart'=>$ticket->dateDepart." a ".$ticket->heureDepart,
                    'arriver'=>$ticket->dateArriver." a ".$ticket->heureArriver,
                ];
                array_push($listInfo, $info);
            }
            $data =
            [
                'Ticket vendus' => count($result),
                'Somme recuperer' => $somme,
                'details'=>$listInfo
            ];
            return $this->successResponse($data);
        }
        return $this->successResponse($result);
    }


    public function pourcentageVendeur(int $prix)
    {
        if ($prix <9999) {
            return 50;
          }
          if ($prix >= 10000 && $prix < 14999) {
            return 100;
          }

          if ($prix >= 15000 && $prix < 19999) {
            return 150;
          }

          if ($prix >= 20000 && $prix < 24999) {
            return 200;
          }

           if ($prix >= 25000 && $prix < 29999) {
            return 250;
          }
          return 0.0;
    }
}
