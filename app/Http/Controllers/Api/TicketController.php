<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\AgenceRepository;
use App\Traits\ApiResponser;
use App\Traits\Frais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TicketController extends Controller
{
    //

    protected $ticketRepo;
    protected $agenceRepo;
    use ApiResponser;
    use Frais;


    function __construct(App $app)
    {
        $this->ticketRepo = new TicketsRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
    }

    /**
     * @OA\Post(
     * path="/api/v1/ticket/find",
     * description="find an ticket",
     * operationId="findticket",
     * tags={"Ticket"},
     *  @OA\Parameter(
     *      name="villedepart",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="villearriver",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="datedepart",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="place",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="poids",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{id: 1,ligne:Lome-Notse,agence:Nagode,prix:4000,place:100,heureDepart:00:18:30,heureArriver:00:20:30,poids: 15,duree:00:30:40}"),     *     )
     *  ),
     * )
     */
    public function find(Request $request)
    {
        $villedepart = $request["villedepart"];
        $villearriver = $request["villearriver"];
        $dateDepart=$request["datedepart"];
        $place = $request["place"];
        $poids = $request["poids"];
        $data = [];
        $find =   $this->ticketRepo->findTicket($villedepart,$villearriver,$dateDepart,$place,$poids);
        foreach ($find as $ticket) {
            $ticketprix = $ticket->prix * $place;
            $totalfrais = ($ticket->frais * $place);
            $frais = $totalfrais + $this->fraisOperateur($ticketprix + $totalfrais);
            $info = [
                'ticket' => $place,
                'find'=>$ticket,
                'ticketprice'=>$ticketprix,
                'ticketfrais'=>$frais,
            ];
            array_push($data,$info);
        }
        return $this->successResponse($data);
    }

    /**
     * @OA\Post(
     * path="/api/v1/ticket/findAgence",
     * description="find an ticket for an agence",
     * operationId="findAgenceTicket",
     * tags={"Ticket"},
     *  @OA\Parameter(
     *      name="agence",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="villedepart",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="villearriver",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="datedepart",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="place",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="poids",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{id: 1,ligne:Lome-Notse,agence:Nagode,prix:4000,place:100,heureDepart:00:18:30,heureArriver:00:20:30,poids: 15,duree:00:30:40}"),     *     )
     *  ),
     * )
     */
    public function findAgence(Request $request)
    {
        $agence =$this->agenceRepo->findname($request["agence"]);
        $villedepart = $request["villedepart"];
        $villearriver = $request["villearriver"];
        $dateDepart=$request["datedepart"];
        $place = $request["place"];
        $poids = $request["poids"];
        $find =   $this->ticketRepo->findTicketAgence($villedepart,$villearriver,$dateDepart,$place,$poids,$agence["id"]);
        foreach ($find as $ticket) {
            $ticketprix = $ticket->prix * $place;
            $totalfrais = ($ticket->frais * $place);
            $frais = $totalfrais + $this->fraisOperateur($ticketprix + $totalfrais);
            $info = [
                'ticket' => $place,
                'find'=>$ticket,
                'ticketprice'=>$ticketprix,
                'ticketfrais'=>$frais,
            ];
            array_push($data,$info);
        }
        return $this->successResponse($data);
    }
}
