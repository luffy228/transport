<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\MobileSoluxRepository;
use App\Repositories\Implementation\MoyenRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class OperateurController extends Controller
{
    //

    protected $moyenRepo;
    protected $mobileSoluxRepo;
    use ApiResponser;

    function __construct(App $app)
    {
        $this->moyenRepo = new MoyenRepository($app);
        $this->mobileSoluxRepo = new MobileSoluxRepository($app);
    }

    /**
     * @OA\Get(
     * path="/api/v1/operateur/liste",
     * description="get all operateur",
     * operationId="getOperateur",
     * tags={"Operateur"},
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{id:1,operateurs:Togocel,ussd:*145*,key:Solde,}"),
     *     )
     *  ),
     * )
     */

    public function liste()
    {
        $data = $this->moyenRepo->all();

        return $this->successResponse($data);
    }

    /**
     * @OA\Post(
     * path="/api/v1/operateur/findMobileMoney",
     * description="choice of the number E-solux who receive the money",
     * operationId="MobileMoneyReceiver",
     * tags={"Operateur"},
     *  @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{keyword:Solde,ussd:*145*,numero:90099000}"),
     *     )
     *  ),
     * )
     */

    public function findMobileMoney(Request $request)
    {
        $infoMobile = $this->moyenRepo->findname($request["name"]);
        $numerorecepteur =$this->mobileSoluxRepo->getNumerobyTypeMoyen('E',$infoMobile["id"]);
        $data = [
                'keyword'=>$infoMobile["key"],
                'ussd'=>$infoMobile["ussd"],
                'numero'=>$numerorecepteur->mobilemoney,
        ];
        return $this->successResponse($data);
    }

}
