<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\ClientsRepository;
use App\Repositories\Implementation\DestinataireInfoRepository;
use App\Repositories\Implementation\LigneRepository;
use App\Repositories\Implementation\MobileSoluxRepository;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use App\Repositories\Implementation\UserRepository;
use App\Repositories\Implementation\VendeurRepository;
use App\Traits\ApiResponser;
use App\Traits\Frais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class StatistiqueSoluxController extends Controller
{
    //
    protected $agenceRepo;
    protected $ligneRepo;
    protected $userRepo;
    protected $vendeurRepo;
    protected $clientRepo;
    protected $destinataireRepo;
    protected $transactionRepo;
    protected $numeroSoluxRepo;
    use Frais;
    use ApiResponser;

    function __construct(App $app)
    {
        $this->agenceRepo = new AgenceRepository($app);
        $this->ligneRepo = new LigneRepository($app);
        $this->userRepo = new UserRepository($app);
        $this->destinataireRepo = new DestinataireInfoRepository($app);
        $this->vendeurRepo = new VendeurRepository($app);
        $this->clientRepo = new ClientsRepository($app);
        $this->transactionRepo = new TransactionUtilisateurRepository($app);
        $this->numeroSoluxRepo = new MobileSoluxRepository($app);
    }

     /**
     * @OA\Get(
     * path="/api/v1/solux/statistique",
     * description="get statistique for the current day",
     * operationId="getStatsToday",
     * tags={"SoluxStats"},
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{agence: 1,ligne: 2,vendeur: 1,destinataire: 1,client:3,gain:2800,transaction:3,frais:60,numeroSolux: [{mobilemoney: 90099000,somme: 21480,operateurs: Togocel}]}"),
     *     )
     *  ),
     * )
     */

    public function statistiqueToday()
    {
        $countAgence = count($this->agenceRepo->all());
        $countLigne = count($this->ligneRepo->all());
        $countVendeur = count($this->vendeurRepo->all());
        $countPuceUser = count($this->destinataireRepo->all());
        $countClient = count($this->clientRepo->all());
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");
        $gainSolux = $this->transactionRepo->gainSoluxHistory($today,$fin);
        $transfert = $this->transactionRepo->transfertHistory($today,$fin);
        $countsms = ($transfert * $this->fraisSms());
        $montantSolux = $this->numeroSoluxRepo->information();
        $data = [
            'agence'=>$countAgence,
            'ligne'=>$countLigne,
            'vendeur'=>$countVendeur,
            'destinataire'=>$countPuceUser,
            'client'=>$countClient,
            'gain'=>$gainSolux,
            'transaction'=>$transfert,
            'frais'=>$countsms,
            'numeroSolux'=>$montantSolux
        ];
        return $this->successResponse($data);

    }

    /**
     * @OA\Post(
     * path="/api/v1/solux/historiqueStats",
     * description="get history of Solux",
     * operationId="historyInformation",
     * tags={"SoluxStats"},
     *  @OA\Parameter(
     *      name="debut",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="fin",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{agence: 1,ligne: 2,vendeur: 1,destinataire: 1,client:3,gain:2800,transaction:3,frais:60,numeroSolux: [{mobilemoney: 90099000,somme: 21480,operateurs: Togocel}]}"),
     *     )
     *  ),
     * )
     */

    public function statistiqueHistory(Request $request)
    {
        $countAgence = count($this->agenceRepo->all());
        $countLigne = count($this->ligneRepo->all());
        $countVendeur = count($this->vendeurRepo->all());
        $countPuceUser = count($this->destinataireRepo->all());
        $countClient = count($this->clientRepo->all());
        $datedebut = strtotime($request['debut']);
        $today = date("Y-m-d H:i:s",$datedebut);
        $datefin = strtotime($request['fin']);
        $fin = date("Y-m-d H:i:s",$datefin);
        $gainSolux = $this->transactionRepo->gainSoluxHistory($today,$fin);
        $transfert = $this->transactionRepo->transfertHistory($today,$fin);
        $countsms = ($transfert * $this->fraisSms());
        $montantSolux = $this->numeroSoluxRepo->information();
        $data = [
            'agence'=>$countAgence,
            'ligne'=>$countLigne,
            'vendeur'=>$countVendeur,
            'destinataire'=>$countPuceUser,
            'client'=>$countClient,
            'gain'=>$gainSolux,
            'transaction'=>$transfert,
            'frais'=>$countsms,
            'numeroSolux'=>$montantSolux
        ];
        return $this->successResponse($data);

    }
}
