<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\TransactionUserJob;
use App\Repositories\Implementation\AgenceFraisRepository;
use App\Repositories\Implementation\AgenceMobileMoneyRepository;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ControlTicketRepository;
use App\Repositories\Implementation\DestinataireInfoRepository;
use App\Repositories\Implementation\MobileSoluxRepository;
use App\Repositories\Implementation\MoyenRepository;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use App\Traits\ExternalApi;
use App\Traits\Frais;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session as FacadesSession;

class TransactionUserController extends Controller
{
    //
    protected $userRepo;
    protected $ticketRepo;
    protected $transactionRepo;
    protected $moyenRepo;
    protected $fraisAgenceRepo;
    protected $agenceRepo;
    protected $controlTicketRepo;
    protected $destinataireInfoRepo;
    protected $mobileSoluxRepo;
    protected $agenceMobileRepo;
    protected $agentRepo;
    use ApiResponser;
    use Frais;
    use ExternalApi;



    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->ticketRepo = new TicketsRepository($app);
        $this->transactionRepo = new TransactionUtilisateurRepository($app);
        $this->moyenRepo = new MoyenRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
        $this->agenceMobileRepo = new AgenceMobileMoneyRepository($app);
        $this->controlTicketRepo = new ControlTicketRepository($app);
        $this->destinataireInfoRepo = new DestinataireInfoRepository($app);
        $this->fraisAgenceRepo = new AgenceFraisRepository($app);
        $this->mobileSoluxRepo = new MobileSoluxRepository($app);
        $this->agentRepo= new AgentRepository($app);
    }

    /**
     * @OA\Post(
     * path="/api/v1/transactionUser/add",
     * description="adding an transaction for an user. Si celui qui lance l'operation est un vendeur alors le num_destinataire est le numero du client dans le cas contraire le num destinataire est identique au num_user",
     * operationId="transactionUser",
     * tags={"TransactionUser"},
     *  @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="ticket_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="operateur",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="nombre_Ticket",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="numeroDestinataire",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{agence: Nagode,ligne: Lome-Sokode,depart: 2021-07-24 - 12:12:00,duree: 15:15:00,place: 1,numeroReservation: 76976}"),
     *     )
     *  ),
     * )
     */

    public function add(Request $request)
    {
        $this->transactionRepo->validateData();
        $listInfo = [];
        $user = $this->userRepo->findNumero(auth()->guard('api')->user()->telephone);
        for ($i=0; $i < $request["nombre_Ticket"]; $i++) {
            $ticket = $this->ticketRepo->information($request["ticket_id"]);
            $moyen = $this->moyenRepo->findname($request["operateur"]);
            $agenceMobilemoney = $this->agenceMobileRepo->informationMobileMoney($ticket->agence_id);
            $numeroSolux = $this->mobileSoluxRepo->getNumerobyTypeMoyen('E',$agenceMobilemoney->moyen_id);
            $pourcentageAgence = $this->fraisAgenceRepo->findAgencebyId($ticket->agence_id);
            $pourcentage =$pourcentageAgence["pourcentage"]/100;
            $gainSolux = (($ticket->prix *$pourcentage) )* $request["nombre_Ticket"];
            $sommeTotal =($ticket->prix + $ticket->frais);

            $envoieMoney = ($ticket->prix - $gainSolux);
            $informationSocket = [
                'operateurs'=>$agenceMobilemoney->operateurs,
                'numero'=>$agenceMobilemoney->mobilemoney,
                'somme'=>(string)$envoieMoney,
                'key'=>$agenceMobilemoney->key,
                'code'=>$agenceMobilemoney->ussd,
                'pin'=>$numeroSolux->pin,
                'nombreTicket'=>1,
            ];
            $nombreTicket = 1;
            $num_reservation = mt_rand(1111111, 9999999);
            $reference = mt_rand(1111111, 9999999);

            $transaction_request = [
                'user_id'=>$user["id"],
                'agence_id'=>$ticket->agence_id,
                'ticket_id'=>$request["ticket_id"],
                'num_user'=>auth()->guard('api')->user()->telephone,
                'num_destinataire'=>$request["numeroDestinataire"],
                'nombre_ticket'=> $nombreTicket,
                'prix'=>$ticket->prix,
                'gainSolux'=>$gainSolux,
                'num_reservation'=>$num_reservation,
                'reference' => $reference,
                'payer' => $request["payer"],
                'ticket_statut'=>'Valider',
                'moyen_id'=>$moyen["id"],
            ];

            if (auth()->guard('api')->user()->telephone != $request["numeroDestinataire"]) {
                $usernum = $this->userRepo->findNumero($request["numeroDestinataire"]);
                if ($usernum == null) {
                    $dest = $this->destinataireInfoRepo->findNumero($request["numeroDestinataire"]);
                    if ($dest == null) {
                        $form_request = [
                            'num_destinataire'=>$request["numeroDestinataire"],
                            'name'=>$request["name"]
                        ];
                        $this->destinataireInfoRepo->create($form_request);
                    }
                }
                $transaction_request["sms"] = "oui";
            }
            if (auth()->guard('api')->user()->telephone == $request["numeroDestinataire"]) {
                $transaction_request["sms"] = "non";
            }
            $info=[

                'agence'=>$ticket->agence,
                'prix'=>$ticket->prix,
                'reference'=>$reference,
                'numero'=>$request["numeroDestinataire"],
                'duree'=>$ticket->duree,
                'ligne'=>$ticket->ligne,
                'depart'=>$ticket->dateDepart." a ".$ticket->heureDepart,
                'arriver'=>$ticket->dateArriver." a ".$ticket->heureArriver,
                'statut' =>'Valider'

            ];

            $this->transactionRepo->create($transaction_request);

            array_push($listInfo, $info);
        }





        $update_ticket=[
            'place' => $ticket->place - $request["nombre_Ticket"]
        ];
        $update_mobileMoney=[
            'somme' => $numeroSolux->somme+$sommeTotal
        ];
        //TransactionUserJob::dispatch($informationSocket,$info,auth()->guard('api')->user()->telephone,$request["numeroDestinataire"],$ticket->agence_id)->delay(60);
        $this->ticketRepo->update($update_ticket,$request["ticket_id"]);
        $this->mobileSoluxRepo->update($update_mobileMoney,$numeroSolux->id);

        return $this->successResponse($listInfo);

        /*

        $this->transactionRepo->validateData();
        $user = $this->userRepo->findNumero(auth()->guard('api')->user()->telephone);
        $ticket = $this->ticketRepo->information($request["ticket_id"]);
        $moyen = $this->moyenRepo->findname($request["operateur"]);
        $agenceMobilemoney = $this->agenceMobileRepo->informationMobileMoney($ticket->agence_id);
        $numeroSolux = $this->mobileSoluxRepo->getNumerobyTypeMoyen('E',$agenceMobilemoney->moyen_id);
        $pourcentageAgence = $this->fraisAgenceRepo->findAgencebyId($ticket->agence_id);
        $pourcentage =$pourcentageAgence["pourcentage"]/100;
        $gainSolux = (($ticket->prix *$pourcentage) +$this->fraisSms()+$this->fraisSocket())* $request["nombre_Ticket"];
        $sommeTotal =($ticket->prix + $ticket->frais)* $request["nombre_Ticket"];
        $prixFrais = $sommeTotal+$this->fraisOperateur($sommeTotal);
        $envoieMoney = ($prixFrais - $gainSolux);
        $informationSocket = [
            'operateurs'=>$agenceMobilemoney->operateurs,
            'numero'=>$agenceMobilemoney->mobilemoney,
            'somme'=>(string)$envoieMoney,
            'key'=>$agenceMobilemoney->key,
            'code'=>$agenceMobilemoney->ussd,
            'pin'=>$numeroSolux->pin,
            'nombreTicket'=>$request["nombre_Ticket"],
        ];
        $nombreTicket = $request["nombre_Ticket"];
        $num_reservation = rand(0000000,9999999);
        $transaction_request = [
            'user_id'=>$user["id"],
            'agence_id'=>$ticket->agence_id,
            'ticket_id'=>$request["ticket_id"],
            'num_user'=>auth()->guard('api')->user()->telephone,
            'num_destinataire'=>$request["numeroDestinataire"],
            'nombre_ticket'=> $nombreTicket,
            'prix'=>$prixFrais,
            'gainSolux'=>$gainSolux,
            'num_reservation'=>$num_reservation,
            'ticket_statut'=>'Valider',
            'moyen_id'=>$moyen["id"],
        ];

        if (auth()->guard('api')->user()->telephone != $request["numeroDestinataire"]) {
            $usernum = $this->userRepo->findNumero($request["numeroDestinataire"]);
            if ($usernum == null) {
                $dest = $this->destinataireInfoRepo->findNumero($request["numeroDestinataire"]);
                if ($dest == null) {
                    $form_request = [
                        'num_destinataire'=>$request["numeroDestinataire"],
                        'name'=>$request["name"]
                    ];
                    $this->destinataireInfoRepo->create($form_request);
                }
            }
            $transaction_request["sms"] = "oui";
        }
        if (auth()->guard('api')->user()->telephone == $request["numeroDestinataire"]) {
            $transaction_request["sms"] = "non";
        }

        $info=[
            'agence'=>$ticket->agence,
            'ligne'=>$ticket->ligne,
            'depart'=>$ticket->dateDepart." - ".$ticket->heureDepart,
            'duree'=>$ticket->duree,
            'place'=>$request["nombre_Ticket"],
            'numeroReservation'=>$num_reservation,
        ];
        $update_ticket=[
            'place' => $ticket->place - $request["nombre_Ticket"]
        ];
        $update_mobileMoney=[
            'somme' => $numeroSolux->somme+$prixFrais
        ];
        TransactionUserJob::dispatch($informationSocket,$info,auth()->guard('api')->user()->telephone,$request["numeroDestinataire"],$ticket->agence_id)->delay(60);
        $this->ticketRepo->update($update_ticket,$request["ticket_id"]);
        $this->mobileSoluxRepo->update($update_mobileMoney,$numeroSolux->id);
        $this->transactionRepo->create($transaction_request);
        return $this->successResponse($info);*/
    }





    /**
     * @OA\Post(
     * path="/api/v1/transactionUser/findReservation",
     * description="finding an reservation number",
     * operationId="findNumeroReservation",
     * tags={"TransactionUser"},
     *  @OA\Parameter(
     *      name="numeroReservation",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{utilisateur:testeur testeur,Agence:Nagode,Ligne:Lome-Notse,Depart:2021-07-06 00:18:30,Place: 1,Statut:Valider,numeroreservation:5142054,duree:00:30:00}"),
     *     )
     *  ),
     * )
     */

    public function findReservation(Request $request)
    {
        $this->transactionRepo->validateData();
        // recuperer l'id de l'agence de celui qui est connecter;

        $agence = $this->agentRepo->findAgence(auth()->guard('api')->user()->user_id);

	if ($request["numeroReservation"] != null) {
		$infoReservation = $this->transactionRepo->findNumeroReservation($request["numeroReservation"],$agence->agence_id);
        }

        if ($request["reference"] != null) {

		$infoReservation = $this->transactionRepo->findReference($request["reference"],$agence->agence_id);
        }


        if ($infoReservation != null) {
            $agence = $this->agenceRepo->show($infoReservation["agence_id"]);
            $user= $this->userReservation($infoReservation["num_destinataire"]);
            $ticket = $this->ticketRepo->informationLigne($infoReservation["ticket_id"]);
            $data = [
                'transaction'=>$infoReservation->id,
                'utilisateur'=>$user["name"],
                'agence'=>$agence["name"],
                'ligne'=>$ticket->ligne,
                'depart'=>$ticket->depart." ".$ticket->heure,
                'duree'=>$ticket->duree,
                'reference'=>$infoReservation["reference"],
		'telephone'=>$infoReservation["num_destinataire"],
                'numeroreservation'=>$infoReservation["num_reservation"],
                'statut'=>$infoReservation["ticket_statut"],

            ];
            return $this->successResponse($data);
        }
        $data = ["utilisateur" => "no"];
        return $this->successResponse($data);
    }

   public function findReservationTelephone(Request $request)
    {
        //$this->transactionRepo->validateData();
        //$event = $this->eventRepo->show($request["event"]);

        //Request["event]
/*
        if ($request["telephone"] != null) {
            $infoReservation = $this->transactionRepo->findNumeroTelephone($request["telephone"] ,$request["event"] );
        }

        $eventsController = EventsController::where('users_id', auth()->guard('api')->user()->id)->where('events_id', $request["event"] )->first();

        if (count($infoReservation)>0 && $eventsController != null) {
            $data = [];

            foreach ($infoReservation as $reservation) {
            $user = $this->userReservation($reservation["num_destinataire"]);
            $infoPlace =DB::table('events_places')->where('id',$reservation["events_Place_id"])->first();

            $placeinfo =DB::table('type_places')->where('id' , $infoPlace->typePlaces_id)->first();

            $dataUser = [
                'transaction' => $reservation["id"],
                'utilisateur' => $user["name"],
                'event' =>  $event["name"],
                'numeroreservation' => $reservation["num_reservation"],
                'telephone' => $reservation["num_destinataire"],
                'place' => $placeinfo->name,
                'reference' => $reservation["reference"],
                'statut' => $reservation["ticket_statut"],
                'sortie' => $reservation["sortie"],
            ];
            array_push($data, $dataUser);
            }


            return $this->successResponse($data);
        }
        $data = ["utilisateur" => "no"];
        return $this->successResponse($data);

*/
    }
    /**
     * Determine the user of an reservation
     */
    public function userReservation($numDestinataire)
    {
        $info = $this->userRepo->findNumero($numDestinataire);
        if ($info == null) {
            $user = $this->destinataireInfoRepo->findNumero($numDestinataire);
                return $user;
        }
        return $info;
    }


    /**
     * @OA\Post(
     * path="/api/v1/transactionUser/ValiderReservation",
     * description="Validate an reservation number",
     * operationId="ValidateReservation",
     * tags={"TransactionUser"},
     *  @OA\Parameter(
     *      name="transaction_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="Ticket Valider"),
     *     )
     *  ),
     * )
     */

    public function validerReservation(Request $request)
    {
        $form_request =[
            'ticket_statut'=>"Expirer"
        ];
        $this->transactionRepo->update($form_request,$request["transaction_id"]);
        $control_request = [
            'user_id'=>auth()->guard('api')->user()->id,
            'transaction_id'=>$request["transaction_id"],
        ];
        $this->controlTicketRepo->create($control_request);
        return $this->successResponse("Ticket Valider");
    }


}
