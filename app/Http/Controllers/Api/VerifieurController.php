<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\ControlTicketRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class VerifieurController extends Controller
{
    protected $userRepo;
    protected $agenceRepo;
    protected $controlTicketRepo;

    use ApiResponser;
    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->controlTicketRepo = new ControlTicketRepository($app);
        $this->agenceRepo = new AgenceRepository($app);

    }

    /**
     * @OA\Post(
     * path="/api/v1/controlleur/login",
     * description="adding an login",
     * operationId="loginControlleur",
     * tags={"controlleur"},
     *  @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{token:'',role:Voyageur,information:{id: 3,name:barry,username:barry,email:barry@gmail.com,telephone:70517794,user_type:,user_id:}"),
     *     )
     *  ),
     * )
     */

    public function login()
    {
        if (Auth::attempt(['username' => request('name'), 'password' => request('password')])) {
            $user = Auth::user();
            $role = $user->getRoleNames();

            if ($role[0] == "Controleur") {
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['role'] = "Controleur";
                $data['information'] = $this->userRepo->Information($user->id);
                return $this->successResponse($data);
            }

            if ($role[0] != "Controleur") {
                return $this->errorResponse('Vous n avez pas le droit necessaire', 402);
            }
        } else {
            return $this->errorResponse('Authentification failled: email or password incorrect', 403);
        }
    }

    /**
     * @OA\Post(
     * path="/api/v1/controlleur/countverifyticket",
     * description="count number of ticket that a controller verify in the day",
     * operationId="countTodayVerification",
     * tags={"controlleur"},
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{ligne:Lome-Sokode,ticket:0,totalTicket:1}"),
     *     )
     *  ),
     * )
     */

    public function verifyticketToday(Request $request)
    {
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");
        $info = $this->controlTicketRepo->ticketControl(auth()->guard('api')->user()->id,$today,$fin);
        $agence_id = $this->userRepo->findAgence(auth()->guard('api')->user()->id);
        if ($agence_id!= null) {
            $listeTicket = $this->agenceRepo->listeTicket($agence_id->agence_id);
            $data = [];
            $somme = 0;
            if ($listeTicket != null) {
                foreach ($listeTicket as $idTicket) {
                    foreach ($info as $control) {
                        if ($idTicket->id == $control->ticket_id) {
                                $somme+=1;
                        }
                    }
                    $countLigne =
                    [
                        'ligne'=>$idTicket->name,
                        'ticket'=>$somme,
                        'totalTicket'=> count($info)
                    ];
                    array_push($data,$countLigne);
                }
                return $this->successResponse($data);
            }
            return $this->successResponse($listeTicket);
        }
        return $this->successResponse($agence_id);
    }

    /**
     * @OA\Post(
     * path="/api/v1/controlleur/historyverifyticket",
     * description="count number of ticket that a controller verify in the history",
     * operationId="countHistoryVerification",
     * tags={"controlleur"},
     *  @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="debut",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="fin",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{ligne:Lome-Sokode,ticket:0,totalTicket:1}"),
     *     )
     *  ),
     * )
     */

    public function Historyverifyticket(Request $request)
    {
        $datedebut = strtotime($request['debut']);
        $debut = date("Y-m-d H:i:s",$datedebut);
        $datefin = strtotime($request['fin']);
        $fin = date("Y-m-d H:i:s",$datefin);
        $info = $this->controlTicketRepo->ticketControl(auth()->guard('api')->user()->id,$debut,$fin);
        $agence_id = $this->userRepo->findAgence(auth()->guard('api')->user()->id);
        if ($agence_id!= null) {
            $listeTicket = $this->agenceRepo->listeTicket($agence_id->agence_id);
            $data = [];
            $somme = 0;
            if ($listeTicket != null) {
                foreach ($listeTicket as $idTicket) {
                    foreach ($info as $control) {
                        if ($idTicket->id == $control->ticket_id) {
                                $somme+=1;
                        }
                    }
                    $countLigne =
                    [
                        'ligne'=>$idTicket->name,
                        'ticket'=>$somme,
                        'totalTicket'=> count($info)
                    ];
                    array_push($data,$countLigne);
                }
                return $this->successResponse($data);
            }
            return $this->successResponse($listeTicket);
        }
        return $this->successResponse($agence_id);
    }



}
