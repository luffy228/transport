<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceMobileMoneyRepository;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\TransactionSoluxRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TransactionSoluxController extends Controller
{
    //
    protected $userRepo;
    protected $ticketRepo;
    protected $transactionSoluxRepo;
    protected $moyenRepo;
    protected $agenceRepo;
    protected $controlTicketRepo;
    protected $destinataireInfoRepo;
    protected $agenceMobileRepo;
    use ApiResponser;



    function __construct(App $app)
    {

        $this->transactionSoluxRepo = new TransactionSoluxRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
        $this->agenceMobileRepo = new AgenceMobileMoneyRepository($app);
    }

    /**
     * @OA\Post(
     * path="/api/v1/transactionSolux/add",
     * description="adding an transaction for an agence.",
     * operationId="transactionSolux",
     * tags={"TransactionSolux"},
     * @OA\Parameter(
     *      name="ticket",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Parameter(
     *      name="prix",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="numero",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *    response=402,
     *    description="Query error",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="error SQL[40220]: ")
     *        )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="Transaction effectuer avec success"),
     *     )
     *  ),
     * )
     */

    public function add(Request $request)
    {

        $this->transactionSoluxRepo->validateData();
        // Recuperer l'agence_id a partir du numero de l'agence
        $agence = $this->agenceMobileRepo->findNumero($request["numero"]);
        $transaction_request = [
                'agence_id'=>$agence["agence_id"],
                'nombreTicket'=>$request["ticket"],
                'prix'=>$request["prix"],
                'numeroAgence'=>$request["numero"],
                'statut'=> "Valider",
        ];
        $this->transactionSoluxRepo->create($transaction_request);
        return $this->successResponse("Transaction effectuer avec success");
    }
}
