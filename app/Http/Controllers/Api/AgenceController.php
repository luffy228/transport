<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\TransactionSoluxRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AgenceController extends Controller
{
    //
    protected $agenceRepo;
    protected $transactionSoluxRepo;
    use ApiResponser;

    function __construct(App $app)
    {
        $this->agenceRepo = new AgenceRepository($app);
        $this->transactionSoluxRepo = new TransactionSoluxRepository($app);

    }



    /**
     * @OA\Get(
     * path="/api/v1/agence/liste",
     * description="get all agence",
     * operationId="getAgence",
     * tags={"Agence"},
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{id:1,name:Nagode,addresse:Agoe,telephone:70517795,mobilemoney:70517795,reseau:Togocel,created_at:2021-07-05T18:22:58.000000Z,updated_at:2021-07-05T18:22:58.000000Z}"),
     *     )
     *  ),
     * )
     */
    public function liste()
    {
        $agence = $this->agenceRepo->all();
        return $this->successResponse($agence);
    }

    /**
     * @OA\Post(
     * path="/api/v1/agence/find",
     * description="finding an agence",
     * operationId="findAgence",
     * tags={"Agence"},
     *  @OA\Parameter(
     *      name="agence",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{id:1,name:Nagode,addresse:Agoe,telephone:70517795,mobilemoney:70517795,reseau:Togocel,created_at:2021-07-05T18:22:58.000000Z,updated_at:2021-07-05T18:22:58.000000Z}"),
     *     )
     *  ),
     * )
     */

    public function findAgence(Request $request)
    {
        $data = $this->agenceRepo->findname($request["agence"]);
        return $this->successResponse($data);
    }

     /**
     * @OA\Post(
     * path="/api/v1/agence/benefice",
     * description="benefice for the current day ",
     * operationId="beneficeAgence",
     * tags={"Agence"},
     *  @OA\Parameter(
     *      name="agence",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{transfert: 0,gain:0}"),
     *     )
     *  ),
     * )
     */

    public function beneficeToday(Request $request)
    {
        $agence = $this->agenceRepo->findname($request["agence"]);
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");
        $countTransaction = $this->transactionSoluxRepo->countAgenceTransfert($agence["id"],$today,$fin) ;
        $countGain = $this->transactionSoluxRepo->countAgenceGain($agence["id"],$today,$fin) ;
        $data = [
            'transfert'=>$countTransaction,
            'gain'=>$countGain,
        ];
        return $this->successResponse($data);


    }

    /**
     * @OA\Post(
     * path="/api/v1/agence/historybenefice",
     * description="history of benefice",
     * operationId="historybeneficeAgence",
     * tags={"Agence"},
     *  @OA\Parameter(
     *      name="agence",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="debut",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="fin",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{transfert: 0,gain:0}"),
     *     )
     *  ),
     * )
     */

    public function beneficeHistory(Request $request)
    {
        $agence = $this->agenceRepo->findname($request["agence"]);
        $datedebut = strtotime($request['debut']);
        $today = date("Y-m-d H:i:s",$datedebut);
        $datefin = strtotime($request['fin']);
        $fin = date("Y-m-d H:i:s",$datefin);
        $countTransaction = $this->transactionSoluxRepo->countAgenceTransfert($agence["id"],$today,$fin) ;
        $countGain = $this->transactionSoluxRepo->countAgenceGain($agence["id"],$today,$fin) ;
        $data = [
            'transfert'=>$countTransaction,
            'gain'=>$countGain,
        ];
        return $this->successResponse($data);

    }
}
