<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\DestinataireInfoRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class DestinataireInfoController extends Controller
{
    //
    protected $destinataireRepo;
    protected $userRepo;
    use ApiResponser;

    function __construct(App $app)
    {
        $this->destinataireRepo = new DestinataireInfoRepository($app);
        $this->userRepo = new UserRepository($app);
    }

    /**
     * @OA\Post(
     * path="/api/v1/destinataire/add",
     * description="add an destinataire information",
     * operationId="addDestinataireInfo",
     * tags={"DestinataireInfo"},
     *  @OA\Parameter(
     *      name="numeroDestinataire",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     *  @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="string"
     *      )
     * ),
     * @OA\Response(
     *     response=201,
     *     description="Success",
     *     @OA\JsonContent(
     *        @OA\Property(property="status", type="string",example="success"),
     *        @OA\Property(property="message", type="string",example="null"),
     *        @OA\Property(property="data", type="string", example="{id:1,name:Nagode,addresse:Agoe,telephone:70517795,mobilemoney:70517795,reseau:Togocel,created_at:2021-07-05T18:22:58.000000Z,updated_at:2021-07-05T18:22:58.000000Z}"),
     *     )
     *  ),
     * )
     */

    public function add(Request $request)
    {
        $this->destinataireRepo->validateData();
        $usernum = $this->userRepo->findNumero($request["numeroDestinataire"]);
        if ($usernum == null) {
            $dest = $this->destinataireRepo->findNumero($request["numeroDestinataire"]);
            if ($dest == null) {
                $form_request = [
                    'num_destinataire'=>$request["numeroDestinataire"],
                    'name'=>$request["name"]
                ];
                $this->destinataireRepo->create($form_request);
                return $this->successResponse("Ajouter avec success");
            }
        }
        return $this->successResponse("Ajouter avec success");


    }
}
