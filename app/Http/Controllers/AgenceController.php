<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\AgenceFraisRepository;
use App\Repositories\Implementation\AgenceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AgenceController extends Controller
{
    //
    protected $agenceRepo;
    protected $agenceFraisRepo;

    function __construct(App $app)
    {
        $this->agenceRepo = new AgenceRepository($app);
        $this->agenceFraisRepo = new AgenceFraisRepository($app);

    }


    public function add(Request $request)
    {

        $this->agenceRepo->validateData();
        $form_request = [
            'name' => $request["name"],
            'addresse' => $request["username"],
            'telephone' => $request["telephone"],
            'mobilemoney' => $request["mobile"],
            'reseau' => $request["reseau"],
        ];
        $this->agenceRepo->create($form_request);
    }

    public function frais(Request $request)
    {

        $this->agenceFraisRepo->validateData();
        $agence = $this->agenceRepo->findname($request["agence"]);
        $form_request = [
            'agence_id' => $agence["id"],
            'pourcentage' => $request["pourcentage"],
        ];
        $this->agenceFraisRepo->create($form_request);
    }

    public function liste()
    {
        dd($this->agenceRepo->all());
    }
}
