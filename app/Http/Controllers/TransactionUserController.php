<?php

namespace App\Http\Controllers;

use Ably\AblyRest;
use Ably\Laravel\AblyServiceProvider;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Ably\Laravel\Facades\Ably as FacadesAbly;
use App\Jobs\TransactionUserJob;
use App\Repositories\Implementation\AgenceRepository;

class TransactionUserController extends Controller
{
    //
    protected $userRepo;
    protected $ticketRepo;
    protected $transactionRepo;
    protected $agenceRepo;



    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->ticketRepo = new TicketsRepository($app);
        $this->transactionRepo = new TransactionUtilisateurRepository($app);
        $this->agenceRepo = new AgenceRepository($app);

    }

    public function add(Request $request)
    {
        $this->transactionRepo->validateData();

        // trouver le user en fonction du telephone

        $user = $this->userRepo->findNumero($request["numero"]);
        $ticket = $this->ticketRepo->information($request["ticket_id"]);
        $nombreTicket = $request["nombre_Ticket"];
        $transaction_request = [
                'user_id'=>$user["id"],
                'agence_id'=>$ticket->agence_id,
                'ticket_id'=>$request["ticket_id"],
                'num_user'=>$request["numero"],
                'num_destinataire'=>$request["numeroDestinataire"],
                'nombre_ticket'=> $nombreTicket,
                'prix'=>$ticket->prix * $nombreTicket,
                'num_reservation'=>$request["numero"].rand(0,9999),
                'transaction_statut'=>"Attente",
                'regle'=>0,
                'ticket_statut'=>'Attente',
        ];
        $this->transactionRepo->create($transaction_request);
    }

    public function update(Request $request)
    {
        $this->transactionRepo->validateData();
        $ably = new AblyRest('hf8FDg.EgVeqw:c6e5zZAau4rLutFt');
        $ably->channel('testchannel')->publish('', 'testPayload');

        /*

        $transaction_id = $this->transactionRepo->findWaitingTransaction($request["numero"]);
        $agence = $this->agenceRepo->show($transaction_id["agence_id"]);
        $information = [
            'agence_id' => $transaction_id["agence_id"],
            'prix' => $transaction_id["prix"],
            'mobilemoney' => $agence["mobilemoney"],
            'reseau' => $agence["reseau"],
            'nombreTicket'=> $transaction_id["nombre_ticket"],
        ];

        $transaction_request = [
            'transaction_statut'=>$request["statut"],
            'ticket_statut'=>'Valide'
        ];
        */
        //$this->transactionRepo->update($transaction_request,$transaction_id["id"]);
        //if ($transaction_id["num_user"] !=$transaction_id["num_destinataire"] ) {
            // API DE SMS AVEC LES INFORMATIONS DE LA TRANSACTION
        //}
        //TransactionUserJob::dispatch($information);


    }
}
