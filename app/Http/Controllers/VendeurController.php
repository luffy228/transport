<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\UserRepository;
use App\Repositories\Implementation\VendeurRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Role;

class VendeurController extends Controller
{
    //
    protected $userRepo;
    protected $vendeurRepo;


    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->vendeurRepo = new VendeurRepository($app);

    }

    public function add(Request $request)
    {
        $this->vendeurRepo->validateData();
        $vendeur_request = [

            'carteIdentite'=>$request["cdn"],
            'cfe'=>$request["cfe"],
        ];
        $vendeur = $this->vendeurRepo->create($vendeur_request);

        $this->userRepo->validateData();
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->vendeurRepo->model(),
            'user_id' => $vendeur['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(5);
        $user->assignRole($role1);
        printf("Vendeur creer avec success");


    }
}
