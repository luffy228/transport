<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AgencyController extends Controller
{
    public function index()
    {
        return view('ui.home');
    }

    public function linesAgency(){
        return view('ui.agency.ligneAgence');
    }

    public function agencyInfos(){
        return view('ui.agency.agencySettings');
    }

     public function agencyPaiements(){
        return view('ui.agency.agencyPaiements');
    }

     public function agencyController()
     {
        return view('ui.agency.agenceControleurs');
     }

}
