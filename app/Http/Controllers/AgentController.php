<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Role;

class AgentController extends Controller
{
    //
    protected $agentRepo;
    protected $agenceRepo;
    protected $userRepo;


    function __construct(App $app)
    {
        $this->agenceRepo = new AgenceRepository($app);
        $this->agentRepo = new AgentRepository($app);
        $this->userRepo = new UserRepository($app);

    }

    public function adminAgence(Request $request)
    {
        $this->agentRepo->validateData();
        $agence = $this->agenceRepo->findname($request["agence"]);
        $agent_request = [
            'agence_id' => $agence["id"],
        ];
        $agent = $this->agentRepo->create($agent_request);

        $this->userRepo->validateData();
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->agentRepo->model(),
            'user_id' => $agent['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(2);
        $user->assignRole($role1);
        printf("administrateur affecter a une agence avec success");
    }

    public function controlleurAgence(Request $request)
    {
        $this->agentRepo->validateData();
        $agence = $this->agenceRepo->findname($request["agence"]);
        $agent_request = [
            'agence_id' => $agence["id"],
        ];
        $agent = $this->agentRepo->create($agent_request);

        $this->userRepo->validateData();
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->agentRepo->model(),
            'user_id' => $agent['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(3);
        $user->assignRole($role1);
        printf("controlleur affecter a une agence avec success");


    }

    public function agentAgence(Request $request)
    {
        $this->agentRepo->validateData();
        $agence = $this->agenceRepo->findname($request["agence"]);
        $agent_request = [
            'agence_id' => $agence["id"],
        ];
        $agent = $this->agentRepo->create($agent_request);

        $this->userRepo->validateData();
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->agentRepo->model(),
            'user_id' => $agent['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(6);
        $user->assignRole($role1);
        printf("agent affecter a une agence avec success");


    }


}
