<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\MoyenRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class OperateurControlleur extends Controller
{
    //
    protected $operateurRepo;


    function __construct(App $app)
    {
        $this->middleware('auth');
        $this->operateurRepo = new MoyenRepository($app);
    }
    protected $rules = [
        'operateurs'=>'required|string',
        'ussd'=>'required|string',
        'key'=>'required|string',
    ];

    protected $rulesussd = [
        'ussd'=>'required|string',
    ];

    protected $ruleskey = [
        'key'=>'required|string',
    ];

    public function store(Request $request)
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if ($valider->fails()) {
            Alert::error("Formulaire incorrect");
            return redirect()->back();
        }
        $operateur_request = [
            'operateurs'=>$request["operateurs"],
            'ussd'=>$request["ussd"],
            'key'=>$request["key"],
        ];
        $this->operateurRepo->createOrIgnore($operateur_request);
        Alert::success("Operateur creer avec success");
        return redirect()->route('operateurall');
    }

    public function updateussd(Request $request)
    {
        $valider =  Validator::make(request()->all(),$this->rulesussd);
        if ($valider->fails()) {
            Alert::error("Formulaire incorrect");
            return redirect()->back();
        }
        $operateur_request = [
            'ussd'=>$request["ussd"],
        ];
        $this->operateurRepo->update($operateur_request,$request["idoperateur"]);
        Alert::success("Code ussd mise a jour avec success");
        return redirect()->route('operateurall');

    }

    public function updatekey(Request $request)
    {
        $valider =  Validator::make(request()->all(),$this->ruleskey);
        if ($valider->fails()) {
            return redirect()->back();
        }
        $operateur_request = [
            'key'=>$request["key"],
        ];
        $this->operateurRepo->update($operateur_request,$request["idoperateur"]);
        Alert::success("Key modifier  avec success");
        return redirect()->route('operateurall');

    }

    public function index()
    {
        $operateur = $this->operateurRepo->all();
        return view('ui.admin.operateur',compact('operateur'));
    }
}
