<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class CompteController extends Controller
{
    protected $agentRepo;
    protected $agenceRepo;
    protected $userRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->agentRepo = new AgentRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
        $this->userRepo = new UserRepository($app);


    }


    public function index()
    {
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $user  = Auth::user();
        $roleliste = $user->getRoleNames();
        $role = $roleliste[0];
        return view('ui.adminAgence.compteInformation',compact('agence','user','role'));
    }

    public function updateAgence(Request $request)
    {
        $update_request=[];
        if ($request["nomAgence"] != null) {
            $update_request["name"] = $request["nomAgence"];
        }
        if ($request["adresseAgence"] != null) {
            $update_request["addresse"] = $request["adresseAgence"];
        }
        if ($request["telephoneAgence"] != null) {
            $update_request["telephone"] = $request["telephoneAgence"];
        }
        $this->agenceRepo->update($update_request,$request["agence_id"]);
        Alert::success("Mise a jour des informations effectuer");
        return redirect()->route('compteinfo');
    }

    public function updateUser(Request $request)
    {
        $update_request=[];
        if ($request["nom"] != null) {
            $update_request["name"] = $request["nom"];
        }
        if ($request["email"] != null) {
            $update_request["email"] = $request["email"];
        }
        if ($request["telephone"] != null) {
            $update_request["telephone"] = $request["telephone"];
        }
        $this->userRepo->update($update_request,Auth::user()->id);
        Alert::success("Mise a jour des informations effectuer");
        return redirect()->route('compteinfo');
    }

    public function updatePassword(Request $request)
    {
        if (Hash::check($request["lastpassword"], Auth::user()->password)) {
            $update_request=[
                'password' => bcrypt($request["newpassword"])
            ];
            $this->userRepo->update($update_request,Auth::user()->id);
            Alert::success("Mot de passe modifier avec success");
            return redirect()->route('compteinfo');

        };
        Alert::error("Mot de passe incorrect");
        return redirect()->back();
    }


}
