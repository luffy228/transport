<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ClientsRepository;
use App\Repositories\Implementation\DestinataireInfoRepository;
use App\Repositories\Implementation\LigneRepository;
use App\Repositories\Implementation\MobileSoluxRepository;
use App\Repositories\Implementation\TransactionSoluxRepository;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use App\Repositories\Implementation\UserRepository;
use App\Repositories\Implementation\VendeurRepository;
use App\Traits\Frais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class GeneriqueController extends Controller
{
    //
    protected $agenceRepo;
    protected $ligneRepo;
    protected $userRepo;
    protected $vendeurRepo;
    protected $clientRepo;
    protected $destinataireRepo;
    protected $transactionRepo;
    protected $transactionSoluxRepo;
    protected $numeroSoluxRepo;
    protected $agentRepo;
    use Frais;

    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->agenceRepo = new AgenceRepository($app);
        $this->agentRepo = new AgentRepository($app);
        $this->ligneRepo = new LigneRepository($app);
        $this->userRepo = new UserRepository($app);
        $this->destinataireRepo = new DestinataireInfoRepository($app);
        $this->vendeurRepo = new VendeurRepository($app);
        $this->clientRepo = new ClientsRepository($app);
        $this->transactionRepo = new TransactionUtilisateurRepository($app);
        $this->transactionSoluxRepo = new TransactionSoluxRepository($app);
        $this->numeroSoluxRepo = new MobileSoluxRepository($app);
    }
    public function index()
    {

        $role = Auth::user()->getRoleNames()[0];
        if ($role == "SuperAdministrateur") {
            $countAgence = count($this->agenceRepo->all());
            $countLigne = count($this->ligneRepo->all());
            $countVendeur = count($this->vendeurRepo->all());
            $countPuceUser = count($this->destinataireRepo->all());
            $countClient = count($this->clientRepo->all());
            $today = date("Y-m-d 00:00:00");
            $fin = date("Y-m-d 23:59:59");
            $gainSolux = $this->transactionRepo->gainSoluxHistory($today,$fin);
            $transfert = $this->transactionRepo->transfertHistory($today,$fin);
            $transfertsms = $this->transactionRepo->transfertHistorySms($today,$fin);
            $montantSolux = $this->numeroSoluxRepo->information();
            $countsms = ($transfertsms * $this->fraisSms());
            $data = [
                'agence'=>$countAgence,
                'ligne'=>$countLigne,
                'vendeur'=>$countVendeur,
                'destinataire'=>$countPuceUser,
                'client'=>$countClient,
                'gain'=>$gainSolux,
                'transaction'=>$transfert,
                'frais'=>$countsms,
                'numeroSolux'=>$montantSolux
            ];

            return view('ui.home',compact('data'));
        }
        if ($role == "AdministrateurAgence") {
            $countLigne = count($this->ligneRepo->all());
            $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
            $today = date("Y-m-d 00:00:00");
            $fin = date("Y-m-d 23:59:59");
            $controlleur = count($this->agenceRepo->getAllController($agence->agence_id)) ;
            $agent = count($this->agenceRepo->getAllAgents($agence->agence_id)) ;
            $countTotal = $this->transactionSoluxRepo->countAgenceTransfert($agence->agence_id,$today,$fin);
            $sommeTotal = $this->transactionSoluxRepo->countAgenceGain($agence->agence_id,$today,$fin);
            $data=[
                'ligne'=>$countLigne,
                'controlleur'=>$controlleur,
                'agent'=>$agent,
                'transaction'=>$countTotal,
                'somme'=>$sommeTotal
            ];

            return view('ui.home',compact('data'));
        }
        if ($role == "Agents") {
            $countLigne = count($this->ligneRepo->all());
            $data=[
                'ligne'=>$countLigne,
            ];
            return view('ui.home',compact('data'));
        }


    }
}
