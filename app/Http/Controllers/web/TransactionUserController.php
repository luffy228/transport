<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TransactionUserController extends Controller
{
    //
    protected $agenceRepo;
    protected $ligneRepo;
    protected $userRepo;
    protected $vendeurRepo;
    protected $clientRepo;
    protected $destinataireRepo;
    protected $transactionRepo;
    protected $numeroSoluxRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
      //  $this->gainAgenceRepo = new AgenceFraisRepository($app);
        //$this->agenceRepo = new AgenceRepository($app);
        //$this->agenceRepo = new AgenceRepository($app);
        //$this->ligneRepo = new LigneRepository($app);
        //$this->userRepo = new UserRepository($app);
        //$this->destinataireRepo = new DestinataireInfoRepository($app);
        //$this->vendeurRepo = new VendeurRepository($app);
        //$this->clientRepo = new ClientsRepository($app);
        $this->transactionRepo = new TransactionUtilisateurRepository($app);
        //$this->numeroSoluxRepo = new MobileSoluxRepository($app);
    }


    public function index()
    {
        //$agencefrais = $this->agenceRepo->gainAgence();
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");
        $countTotal = $this->transactionRepo->transfertHistory($today,$fin);
        $gainSoluxTotal = $this->transactionRepo->gainSoluxHistory($today,$fin);
        $sommeTotal = $this->transactionRepo->moneyReceiveHistory($today,$fin);
        $info = $this->transactionRepo->InformationtransfertHistory($today,$fin);
        return view('ui.admin.transactionUserToday',compact('countTotal','sommeTotal','gainSoluxTotal','info'));
    }
}
