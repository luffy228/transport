<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceMobileMoneyRepository;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\MoyenRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class AgenceMobileMoneyController extends Controller
{
    //
    protected $agenceRepo;
    protected $agentRepo;
    protected $agenceMobileRepo;
    protected $moyenRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->agenceRepo = new AgenceRepository($app);
        $this->agentRepo = new AgentRepository($app);
        $this->agenceMobileRepo = new AgenceMobileMoneyRepository($app);
        $this->moyenRepo = new MoyenRepository($app);
    }

    protected $rules = [
        'mobilemoney'=>'required|string|unique:App\Models\AgenceMobilemoney,mobilemoney|Min:8|Max:8',

    ];


    public function listeMobileNumber ()
    {
        // retourner l'agence_id dans la vue
        // retourner la liste des operateurs
        $moyen = $this->moyenRepo->all();
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $info = $this->agenceMobileRepo->getNumerobyagence($agence->agence_id);
        return view('ui.adminAgence.ParametrageNumeroList',compact('info','agence','moyen'));
    }

    public function MobileNumberadd(Request $request)
    {

        $valider =  Validator::make(request()->all(),$this->rules);
        if ($valider->fails()) {
            Alert::success("Formulaire erroner");
            return redirect()->back();
        }
        $mobile_request = [
            'agence_id'=>$request["agence_id"],
            'mobilemoney'=>$request["mobilemoney"],
            'moyen_id'=>$request["moyen"],
        ];
        $this->agenceMobileRepo->createOrIgnore($mobile_request);
        Alert::success("Numero Agence  creer avec success");
        return redirect()->route('parametragemoneyall');
    }
}
