<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\UserRepository;
use App\Repositories\Implementation\VendeurRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Spatie\Permission\Models\Role;

class VendeurController extends Controller
{
    //protected $agenceRepo;
    //protected $agenceFraisRepo;
    //protected $moyenRepo;
    //protected $agenceMobileMoneyRepo;
    //protected $agentRepo;
    protected $userRepo;
    protected $vendeurRepo;

    public function __construct(App $app)
    {
        $this->middleware('auth');
        //$this->moyenRepo = new MoyenRepository($app);
        //$this->agenceRepo = new AgenceRepository($app);
        //$this->agentRepo = new AgentRepository($app);
        //$this->agenceFraisRepo = new AgenceFraisRepository($app);
        //$this->agenceMobileMoneyRepo = new AgenceMobileMoneyRepository($app);
        $this->userRepo = new UserRepository($app);
        $this->vendeurRepo = new VendeurRepository($app);

    }

    protected $rulesvendeurAdd = [
        'name'=>'required|unique:App\Models\User,name',
        'username'=>'required|unique:App\Models\User,username',
        'email'=>'required|unique:App\Models\User,email',
        'telephone'=>'required|unique:App\Models\User,telephone',
    ];


    public function index()
    {
        // retourner la liste des vendeurs
        $responsable =$this->vendeurRepo->getAllVendeurs();
        return view('ui.admin.vendeurList',compact('responsable'));
    }



    public function formulaire()
    {
        return view('ui.admin.vendeurAdd');
    }


    public function store(Request $request)
    {
        $validerAgence =  Validator::make(request()->all(),$this->rulesvendeurAdd);
        if ($validerAgence->fails()) {
            Alert::error("Formulaire non valide");
            return redirect()->back();
        }
        $vendeur_request = [
            'carteIdentite'=>$request["cdn"],
            'cfe'=>$request["cfe"],
        ];
        $vendeur = $this->vendeurRepo->create($vendeur_request);
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->vendeurRepo->model(),
            'user_id' => $vendeur['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(5);
        $user->assignRole($role1);
        Alert::success("Vendeur creer avec success");
        return redirect()->route('vendeurall');
    }


}
