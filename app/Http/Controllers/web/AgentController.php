<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Spatie\Permission\Models\Role;

class AgentController extends Controller
{
    //
    protected $agenceRepo;
    protected $agentRepo;
    protected $userRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->agenceRepo = new AgenceRepository($app);
        $this->agentRepo = new AgentRepository($app);
        $this->userRepo = new UserRepository($app);

    }

    protected $rulescontrollerAdd = [
        'name'=>'required|unique:App\Models\User,name',
        'username'=>'required|unique:App\Models\User,username',
        'email'=>'required|unique:App\Models\User,email',
        'telephone'=>'required|unique:App\Models\User,telephone',
    ];


    public function indexController()
    {

        // Trouver l'agence liee a cet user;
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $responsable = $this->agenceRepo->getAllController($agence->agence_id);
        return view('ui.adminAgence.controlleurList',compact('responsable'));
    }

    public function indexAgent()
    {

        // Trouver l'agence liee a cet user;
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $responsable = $this->agenceRepo->getAllAgents($agence->agence_id);
        return view('ui.adminAgence.agentList',compact('responsable'));
    }

    public function formulaireController()
    {
        return view('ui.adminAgence.controllerAdd');
    }

    public function formulaireAgent()
    {
        return view('ui.adminAgence.agentAdd');
    }

    public function storeController(Request $request)
    {
        $validerAgence =  Validator::make(request()->all(),$this->rulescontrollerAdd);
        if ($validerAgence->fails()) {
            Alert::error("Formulaire incorrect");
            return redirect()->back();
        }
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $agent_request = [
            'agence_id' => $agence->id,
        ];
        $agent = $this->agentRepo->create($agent_request);
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->agentRepo->model(),
            'user_id' => $agent['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(3);
        $user->assignRole($role1);
        Alert::success("Controlleur creer avec success");
        return redirect()->route('controllerall');
    }

    public function storeAgent(Request $request)
    {
        $validerAgence =  Validator::make(request()->all(),$this->rulescontrollerAdd);
        if ($validerAgence->fails()) {
            Alert::error("Formulaire incorrect");
            return redirect()->back()->withErrors($validerAgence->errors());
        }
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $agent_request = [
            'agence_id' => $agence->id,
        ];
        $agent = $this->agentRepo->create($agent_request);
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->agentRepo->model(),
            'user_id' => $agent['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(6);
        $user->assignRole($role1);
        Alert::success("Agent creer avec success");
        return redirect()->route('agentall');
    }
}
