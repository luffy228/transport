<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Traits\Frais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FraisController extends Controller
{
    //
    use Frais;



    function __construct(App $app)
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $frais = [
            'sms'=> $this->fraisSms(),
            'socket'=> $this->fraisSocket(),
        ];
        return view('ui.admin.fraisList',compact('frais'));
    }
}
