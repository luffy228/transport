<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\MobileSoluxRepository;
use App\Repositories\Implementation\MoyenRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class NumeroSoluxController extends Controller
{
    //

    protected $numeroSolux;
    protected $operateurRepo;

    function __construct(App $app)
    {
        $this->middleware('auth');
        $this->numeroSolux = new MobileSoluxRepository($app);
        $this->operateurRepo = new MoyenRepository($app);
    }

    protected $rules = [
        'mobilemoney'=>'required|string:Min:8|Max:8',
        'pin'=>'required|string:Min:4|Max:4'
    ];

    protected $rulespin = [
        'pin'=>'required|string:Min:4|Max:4'
    ];

    public function store(Request $request)
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if ($valider->fails()) {
            Alert::error("Formulaire incorrect");
            return redirect()->back();
        }

        $mobile_request = [
            'mobilemoney'=>$request["mobilemoney"],
            'somme'=>$request["somme"],
            'moyen_id'=>$request["moyen"],
            'type'=>$request["type"],
            'pin'=>$request["pin"],
        ];
        $record = $this->numeroSolux->findNumero($request["mobilemoney"]);
        $operateur = $this->numeroSolux->getNumerobyTypeMoyen('E',$request["moyen"]);
        if ($record != null || $operateur != null) {
            Alert::error("Cet operateur a deja un numero ou cet numero existe deja");
            return redirect()->back();
        }
        if ($operateur == null) {
            if ($record == null) {
                $this->numeroSolux->createOrIgnore($mobile_request);
                Alert::success("Numero Solux creer avec success");
                return redirect()->route('numeroSoluxall');
            }

        }

    }

    public function updatesomme(Request $request)
    {
        $mobile_request = [
            'somme'=>$request["somme"],
        ];
        $this->numeroSolux->update($mobile_request,$request["idmobile"]);
        Alert::success("Somme modifier avec succes");
        return redirect()->route('numeroSoluxall');

    }

    public function updatepin(Request $request)
    {
        $valider =  Validator::make(request()->all(),$this->rulespin);
        if ($valider->fails()) {
            Alert::error("Formulaire incorrect");
            return redirect()->back();
        }
        $mobile_request = [
            'pin'=>$request["pin"],
        ];
        $this->numeroSolux->update($mobile_request,$request["idmobile"]);
        Alert::success("Pin modifier avec success");
        return redirect()->route('numeroSoluxall');

    }

    public function index()
    {
        $numero = $this->numeroSolux->all();
        $moyen = $this->operateurRepo->all();
        return view('ui.admin.numeroSoluxList',compact('numero','moyen'));
    }
}
