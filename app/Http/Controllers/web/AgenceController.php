<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceFraisRepository;
use App\Repositories\Implementation\AgenceMobileMoneyRepository;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\MoyenRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use RealRashid\SweetAlert\SweetAlertServiceProvider;
use Spatie\Permission\Models\Role;

class AgenceController extends Controller
{
    //
    protected $agenceRepo;
    protected $agenceFraisRepo;
    protected $moyenRepo;
    protected $agenceMobileMoneyRepo;
    protected $agentRepo;
    protected $userRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->moyenRepo = new MoyenRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
        $this->agentRepo = new AgentRepository($app);
        $this->agenceFraisRepo = new AgenceFraisRepository($app);
        $this->agenceMobileMoneyRepo = new AgenceMobileMoneyRepository($app);
        $this->userRepo = new UserRepository($app);
    }

    protected $rulesagenceAdd = [
        'agenceName'=>'required|unique:App\Models\Agence,name',
        'agenceAdresse'=>'required|string',
        'agenceTelephone'=>'required|unique:App\Models\Agence,telephone|string|Min:8|Max:8',
        'agencemobilemoney'=>'required|unique:App\Models\AgenceMobilemoney,mobilemoney',
        'name'=>'required|unique:App\Models\User,name',
        'username'=>'required|unique:App\Models\User,username',
        'email'=>'required|unique:App\Models\User,email',
        'telephone'=>'required|unique:App\Models\User,telephone',
    ];


    public function index()
    {
        $agence = $this->agenceRepo->all();
        $info =[];
        foreach ($agence as $allagence) {
            $responsable = $this->agenceRepo->findResponsable($allagence["id"]);
            array_push($info,$responsable);
        }
        return view('ui.admin.agenceList',compact('info'));
    }

    public function formulaire()
    {
        $moyen = $this->moyenRepo->all();
        return view('ui.admin.agenceAdd',compact('moyen'));
    }

    public function store(Request $request)
    {

        $validerAgence =  Validator::make(request()->all(),$this->rulesagenceAdd);
        if ($validerAgence->fails()) {
            Alert::error("Formulaire non valide");
            return redirect()->back();
        }
        $agence_request = [
            'name'=>$request["agenceName"],
            'addresse'=>$request["agenceAdresse"],
            'telephone'=>$request["agenceTelephone"],
        ];
        $agence = $this->agenceRepo->create($agence_request);
        $agencefrais_request = [
            'agence_id'=>$agence["id"],
            'pourcentage'=>$request["pourcentage"],
        ];
        $this->agenceFraisRepo->create($agencefrais_request);

        $agencemobile_request = [
            'agence_id'=>$agence["id"],
            'mobilemoney'=>$request["agencemobilemoney"],
            'moyen_id'=>$request["moyen"],
        ];
        $this->agenceMobileMoneyRepo->create($agencemobile_request);
        $agent_request = [
            'agence_id' => $agence["id"],
        ];
        $agent = $this->agentRepo->create($agent_request);
        $form_request = [
            'name' => $request["name"],
            'username' => $request["username"],
            'password' => bcrypt($request["password"]),
            'email' => $request["email"],
            'telephone' => $request["telephone"],
            'user_type' => $this->agentRepo->model(),
            'user_id' => $agent['id'],
        ];
        $user = $this->userRepo->create($form_request);
        $role1 = Role::find(2);
        $user->assignRole($role1);
        Alert::success("Agence creer avec success");
        return redirect()->route('agenceall');

    }

}
