<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ClientsRepository;
use App\Repositories\Implementation\DestinataireInfoRepository;
use App\Repositories\Implementation\LigneRepository;
use App\Repositories\Implementation\MobileSoluxRepository;
use App\Repositories\Implementation\TransactionSoluxRepository;
use App\Repositories\Implementation\TransactionUtilisateurRepository;
use App\Repositories\Implementation\UserRepository;
use App\Repositories\Implementation\VendeurRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TransactionSoluxController extends Controller
{
    //
    protected $agenceRepo;
    protected $ligneRepo;
    protected $userRepo;
    protected $vendeurRepo;
    protected $clientRepo;
    protected $destinataireRepo;
    protected $transactionRepo;
    protected $numeroSoluxRepo;
    protected $agentRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->agenceRepo = new AgenceRepository($app);
        $this->ligneRepo = new LigneRepository($app);
        $this->userRepo = new UserRepository($app);
        $this->transactionRepo = new TransactionSoluxRepository($app);
        $this->numeroSoluxRepo = new MobileSoluxRepository($app);
        $this->agentRepo = new AgentRepository($app);
    }


    public function index()
    {
        //$agencefrais = $this->agenceRepo->gainAgence();
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");
        $countTotal = $this->transactionRepo->transfertHistory($today,$fin);
        $sommeTotal = $this->transactionRepo->moneySendSoluxHistory($today,$fin);
        $info = $this->transactionRepo->InformationtransfertHistory($today,$fin);
        return view('ui.admin.transactionSoluxToday',compact('countTotal','sommeTotal','info'));
    }

    public function agenceTransaction()
    {
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $today = date("Y-m-d 00:00:00");
        $fin = date("Y-m-d 23:59:59");
        $countTotal = $this->transactionRepo->countAgenceTransfert($agence->agence_id,$today,$fin);
        $sommeTotal = $this->transactionRepo->countAgenceGain($agence->agence_id,$today,$fin);
        $info = $this->transactionRepo->AgenceInformationtransfertHistory($agence->agence_id,$today,$fin);
        return view('ui.adminAgence.transactionRecuAgence',compact('countTotal','sommeTotal','info'));
    }
}
