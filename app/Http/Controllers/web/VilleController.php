<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\VilleRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use RealRashid\SweetAlert\Facades\Alert;

class VilleController extends Controller
{
    //
    protected $villeRepo;


    function __construct(App $app)
    {
        $this->middleware('auth');
        $this->villeRepo = new VilleRepository($app);
    }
    protected $rules = [
        'ville'=>'required|string|Min:3',
    ];


    public function store(Request $request)
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if ($valider->fails()) {
            Alert::error("formulaire incorrect");
            return redirect()->back();
        }
        $ville_request = [
            'nom'=>$request["ville"],
        ];
        $this->villeRepo->createOrIgnore($ville_request);
        Alert::success("Ville creer avec success");
        return redirect()->route('villeall');
    }

    public function index()
    {
        $ville = $this->villeRepo->all();
        return view('ui.admin.villeList',compact('ville'));
    }
}
