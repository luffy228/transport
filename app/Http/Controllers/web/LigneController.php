<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceLigneRepository;
use App\Repositories\Implementation\AgenceRepository;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\LigneRepository;
use App\Repositories\Implementation\TicketsRepository;
use App\Repositories\Implementation\VilleRepository;
use App\Traits\Frais;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class LigneController extends Controller
{
    //
    protected $agentRepo;
    protected $agenceRepo;
    protected $ticketRepo;
    protected $villeRepo;
    protected $ligneRepo;
    protected $agenceligneRepo;
    use Frais;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->agentRepo = new AgentRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
        $this->ticketRepo = new TicketsRepository($app);
        $this->villeRepo = new VilleRepository($app);
        $this->ligneRepo = new LigneRepository($app);
        $this->agenceligneRepo = new AgenceLigneRepository($app);
    }


    public function index ()
    {
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $ticket = $this->agenceRepo->listeTicket($agence->agence_id);
        return view('ui.adminAgence.ligneList',compact('ticket'));
    }

    public function ticket ()
    {
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        $ticket = $this->agenceRepo->listeTicket($agence->agence_id);
        return view('ui.adminAgence.ticketList',compact('ticket'));
    }

    public function formligne()
    {
        $ville = $this->villeRepo->all();
        return view('ui.adminAgence.ligneAdd',compact('ville'));
    }

    public function store(Request $request)
    {

        if ($request["depart"] == $request["arriver"]) {
            Alert::error("Le point de depart doit etre different du point d'arriver");
            return redirect()->back();
        }
        $firstVille = $this->villeRepo->findname($request["depart"]);
        $secondVille =  $this->villeRepo->findname($request["arriver"]);
        $ligneName =$request["depart"]."-".$request["arriver"];
        $ligne_request = [
            'ville_depart_id'=>$firstVille->id,
            'ville_arriver_id'=>$secondVille->id,
            'name'=> $ligneName,
        ];
        $frais = $this->fraisSms()+$this->fraisSocket();
        $agence = $this->agentRepo->findAgence(Auth::user()->user_id);
        if ($request["datedepart"] > $request["datearriver"]) {
            Alert::error("date de depart erroner");
            return redirect()->back();
        }
        if ($request["datedepart"] == $request["datearriver"]) {
            if ($request["timedepart"] >= $request["timearriver"]) {
                Alert::error("heure de depart erroner");
                return redirect()->back();
            }
        }
        $ligne = $this->ligneRepo->createOrIgnore($ligne_request);
        $agenceLigne_request = [
            'agence_id'=>$agence->id,
            'ligne_id'=>$ligne["id"],
        ];

        $date1 =  new DateTime($request["datedepart"]." ".$request["timedepart"]);
        $date2 =  new DateTime($request["datearriver"]." ".$request["timearriver"]);
        $interval = $date1->diff($date2);
        $totalHours = $interval->m * 30 * 24 + $interval->d * 24 + $interval->h ;
        $totalMinutes =$interval->i;
        $totalSeconds = $interval->s;
        $duree = "$totalHours".":"."$totalMinutes".":"."$totalSeconds";
        $agenceLigne = $this->agenceligneRepo->create($agenceLigne_request);
        $ticket_request = [
            'agence_ligne_id'=>$agenceLigne["id"],
            'heureDepart'=>$request["timedepart"],
            'heureArriver'=>$request["timearriver"],
            'dateDepart'=>$request["datedepart"],
            'dateArriver'=>$request["datearriver"],
            'prix'=> $request["prix"],
            'frais'=>$frais,
            'duree'=>$duree,
            'place'=> $request["totalplace"],
            'totalPlace'=> $request["totalplace"],
            'poids'=> $request["poids"],
        ];
        $this->ticketRepo->create($ticket_request);
        Alert::success("Ticket creer avec success");
        return redirect()->route('ligneall');
    }

}
