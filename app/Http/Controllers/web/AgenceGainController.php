<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Repositories\Implementation\AgenceFraisRepository;
use App\Repositories\Implementation\AgenceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AgenceGainController extends Controller
{
    //
    protected $gainAgenceRepo;
    protected $agenceRepo;
    public function __construct(App $app)
    {
        $this->middleware('auth');
        $this->gainAgenceRepo = new AgenceFraisRepository($app);
        $this->agenceRepo = new AgenceRepository($app);
    }


    public function index()
    {
        // recuperer la liste des agences
        // Pour chaque agence combiner son responsable
        $agencefrais = $this->agenceRepo->gainAgence();

        return view('ui.admin.agencegainList',compact('agencefrais'));
    }
}
