<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function paiement()
    {
        return view('ui.admin.adminPaiements');
    }

    public function agencyList()
    {
        return view('ui.admin.agencyList');
    }

    public function settings()
    {
        return view('ui.admin.adminSettings');
    }
}
