<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\VilleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class VilleController extends Controller
{
    //

    protected $villeRepo;


    function __construct(App $app)
    {
        $this->villeRepo = new VilleRepository($app);
    }

    public function add(Request $request)
    {
        $this->villeRepo->validateData();
        $ville_request = [
            'nom'=>$request["nom"],
        ];
        $this->villeRepo->create($ville_request);
        printf("Ville creer avec succes");


    }
}
