<?php

namespace App\Http\Middleware;

use Closure;

class ForceJsonResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       /*  return $next($request)
            ->header('Accept', 'application/json')
            ->header('Content-type', 'application/json'); */
            
        $request->headers->set('Accept', 'application/json');
        //$request->headers->set('Content-type', 'application/json');
        return $next($request);
    }
}
