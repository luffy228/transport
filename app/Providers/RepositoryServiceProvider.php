<?php

namespace App\Providers;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Repositories\Generic\GenericInterface\GenericRepositoryInterface;
use App\Repositories\Implementation\UserRepository;
use App\Repository\ModelInterfaces\LoginRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GenericRepositoryInterface::class, GenericRepository::class);
        $this->app->bind(LoginRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
