@extends('layouts.app')

@section('content')
<div id="auth">
    <div class="container">
        @include('sweetalert::alert')
        <div class="row">
            <div class="col-md-5 col-sm-12 mx-auto mt-5">
                <div class="card pt-4">
                    <div class="card-body">
                        <div class="text-center mb-5">
                            <img src="assets/images/favicon.svg" height="48" class='mb-4'>
                            <h3>Connexion </h3>
                            <p>Saisissez vos identifiants de connexion</p>
                        </div>
                        <form method="POST" action="{{ route('SubmitLogin') }}">
                            @csrf()
                            <div class="form-group position-relative ">
                                <label for="username">Identifiant</label>
                                <div class="mt-3">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                </div>
                            </div>
                            <div class="form-group  ">
                                <div class="clearfix">
                                    <label for="password">Mot de passe</label>

                                </div>
                                <div class="mb-3 mt-2">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="col-md-7 offset-3" >
                                <button class="btn btn-primary btn-lg" type="submit">Me connecter</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

        </div>
@endsection
