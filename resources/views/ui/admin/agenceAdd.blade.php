@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Ajouter une agence
        </div>
        <div class="card-body">
            <form class="form" method="POST" action="{{route('agenceadd')}}">
                @csrf
                <section id="multiple-column-form">
                    <div class="row match-height">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Information Agence</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">

                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="first-name-column">Nom de l'agence</label>
                                                        <input type="text" id="first-name-column" class="form-control" placeholder="Nom de l'agence"
                                                            name="agenceName" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Adresse de l'agence</label>
                                                        <input type="text" id="last-name-column" class="form-control" placeholder="Adresse"
                                                            name="agenceAdresse" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Contact de l'agence</label>
                                                        <input type="text" id="country-floating" class="form-control" name="agenceTelephone"
                                                            placeholder="contact de l'agence" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="company-column">Pourcentage sur les tickets</label>
                                                        <input type="number" id="company-column" class="form-control" name="pourcentage"
                                                            placeholder="pourcentage" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="city-column">Operateur</label>
                                                        <select class="form-control" name="moyen" >
                                                            <option value="" disabled>--Choix de l'operaterateur--</option>
                                                            @foreach ($moyen as $moyen)
                                                            <option value={{$moyen->id}}>{{$moyen->operateurs}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Numero mobile money</label>
                                                        <input type="text" id="country-floating" class="form-control" name="agencemobilemoney"
                                                            placeholder="mobilemoney" required>
                                                    </div>
                                                </div>

                                            </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="multiple-column-form">
                    <div class="row match-height">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Gerant de l'agence</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="first-name-column">Noms et prenoms</label>
                                                        <input type="text" id="first-name-column" class="form-control" placeholder="nom et prenoms"
                                                            name="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Nom d'utilisateur</label>
                                                        <input type="text" id="last-name-column" class="form-control" placeholder="noms d'utilisateur"
                                                            name="username" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="city-column">Mot de passe</label>
                                                        <input type="text" id="city-column" class="form-control" placeholder="mot de passe" name="password" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="email-id-column">Email</label>
                                                        <input type="email" id="email-id-column" class="form-control" name="email"
                                                            placeholder="Email" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Numero de telephone</label>
                                                        <input type="text" id="country-floating" class="form-control" name="telephone"
                                                            placeholder="Numero de telephone du gerant" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                    <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>


        </div>
    </div>

</section>
@endsection
