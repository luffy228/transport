@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste des villes
        </div>
        <div class="col-12 col-md-12 ml-3 mr-5">
            <div class="modal fade" id="primary" tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <h5 class="modal-title white" id="myModalLabel160">Ajouter une Ville</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i data-feather="x"></i>
                                                </button>
                                            </div>
                                            <form method="post" action="{{ route('villeadd') }}">
                                                @csrf
                                                    <div class="modal-body">
                                                            <div class="form-body">
                                                                <div class="form-group">
                                                                    <label for="feedback1" class="sr-only">Nom de la ville</label>
                                                                    <input type="text" id="feedback1" class="form-control" placeholder="Nom de la ville" name="ville" required>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button  type="submit" class="btn btn-primary" >Valider</button>
                                                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                                    </div>
                                            </form>
                                    </div>
                                </div>
                    </div>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#primary">
                Ajouter une ville
            </button>
            <p class="card-text">
                La liste des villes enregistrees sur notre plateforme.
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($ville as $ville)
                        <tr>
                            <td>{{$ville["nom"]}}</td>
                            <td>Supprimer ville</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
