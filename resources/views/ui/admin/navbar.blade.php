<li class="sidebar-item">
    <a href="{{ route('accueil') }}" class='sidebar-link'>
        <span>{{Auth::user()->name}}</span>
    </a>
</li>

<li class="sidebar-item">
    <a href="{{ route('accueil') }}" class='sidebar-link'>

        <span>Role:{{Auth::user()->getRoleNames()[0]}}</span>
    </a>
</li>
<li class="sidebar-item">
    <a href="{{ route('accueil') }}" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Acceuil</span>
    </a>
</li>

<li class="sidebar-item">
    <a href="{{ route('agenceall') }}" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Liste des Agences</span>
    </a>
</li>

<li class="sidebar-item">
    <a href="{{ route('vendeurall') }}" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Liste des vendeurs</span>
    </a>
</li>

<li class="sidebar-item">
    <a href="{{ route('villeall') }}" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Liste ville</span>
    </a>
</li>

<li class="sidebar-item">
    <a href="{{ route('numeroSoluxall') }}" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Liste Numero E-Solux</span>
    </a>
</li>

<li class="sidebar-item">
    <a href="{{ route('operateurall') }}" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Liste Moyens de paiements</span>
    </a>
</li>

<li class='sidebar-title'>Transactions</li>
    <li class="sidebar-item  has-sub">
        <a href="#" class="sidebar-link">
            <span>Envoyes</span>
        </a>
        <ul class="submenu ">
            <li>
                <a href="{{ route('transactionsoluxToday') }}">Aujourd'hui</a>
            </li>
            <li>
                <a href="#">Historique</a>
            </li>
        </ul>
    </li>

    <li class="sidebar-item  has-sub">
        <a href="#" class="sidebar-link">
            <span>Recu</span>
        </a>
        <ul class="submenu ">
            <li>
                <a href="{{ route('transactionuserToday') }}">Aujourd'hui</a>
            </li>
            <li>
                <a href="#">Historique</a>
            </li>
        </ul>
    </li>
</li>

<li class='sidebar-title'>Finances</li>

    <li class="sidebar-item  has-sub">
        <a href="#" class="sidebar-link">
            <span>Finances</span>
        </a>
        <ul class="submenu ">
            <li>
                <a href="{{ route('fraisall') }}">Liste des frais</a>
            </li>
            <li>
                <a href="{{ route('gainagence') }}">Gains agences</a>
            </li>

            <li>
                <a href="#">Bilan</a>
            </li>
        </ul>
    </li>
</li>


<li class="sidebar-item">
    <a href="#" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Statistiques</span>
    </a>
</li>

<li class="sidebar-item">
    <a href="{{ route('Logout') }}" class='sidebar-link'>
        <i data-feather="home" width="20"></i>
        <span>Se deconnecter</span>
    </a>
</li>



