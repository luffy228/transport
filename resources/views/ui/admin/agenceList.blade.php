@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste des agences
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-outline-primary float-right">
                <a href="{{route('agenceform')}}">Ajouter une agence</a>

            </button>
            <p class="card-text">
                La liste des agences enregistrees sur notre plateforme.
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Agence</th>
                        <th>Information</th>
                        <th>MobileMoney</th>
                        <th>Responsable</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($info as $agence)
                        <tr>
                            <td>{{$agence->agence}}</td>
                            <td>Adresse :{{$agence->adresse}} / Telephone : {{$agence->numero}} </td>
                            <td> Numero Mobile Money :
                                <?php $mobilemoney = DB::table('agence_mobilemoneys')
                                                        ->where('agence_id',$agence->agence_id)
                                                        ->get() ?>
                                @foreach ($mobilemoney as $agenceNumber)
                                    {{$agenceNumber->mobilemoney}} -
                                @endforeach
                            </td>
                            <td>{{$agence->responsable}}</td>
                            <td>Changer Administrateur / Supprimer Agence</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
