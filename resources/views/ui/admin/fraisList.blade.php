@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste de nos frais
        </div>

        <div class="card-body">
            <div class="col-12 col-md-3">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class='card-title'>Cout</h3>
                                <div class="card-right d-flex align-items-center">
                                    <p> Sms : {{$frais['sms']}} Francs CFA</p>
                                    <p> Socket : {{$frais['sms']}} Francs CFA</p>
                                </div>
                            </div>
                            <div class="chart-wrapper">
                                <canvas id="canvas1" style="height:100px !important"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

</section>
@endsection
