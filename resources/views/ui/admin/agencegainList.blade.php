@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste des pourcentages avec les agences
        </div>
        <div class="card-body">
            <p class="card-text">
                Liste des pourcentages de E-solux sur chaque Ticket d'une agence
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Agence</th>
                        <th>Pourcentage</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($agencefrais as $gain)
                        <tr>
                            <td>{{$gain->agence}}</td>
                            <td>{{$gain->pourcentage}}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
