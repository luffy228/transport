@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste des Operateurs
        </div>
        <div class="col-12 col-md-12 ml-3 mr-5">
            <div class="modal fade" id="primary" tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <h5 class="modal-title white" id="myModalLabel160">Ajouter un Operateur</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i data-feather="x"></i>
                                                </button>
                                            </div>
                                            <form class="form" method="post" action="{{ route('operateuradd') }}">
                                                @csrf
                                                <div class="modal-body">

                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label for="feedback1" class="sr-only">Nom de l'operateur</label>
                                                            <input type="text" id="feedback1" class="form-control" placeholder="Nom de l'operateur" name="operateurs" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="feedback4" class="sr-only">Code ussd</label>
                                                            <input type="text" id="feedback4" class="form-control" placeholder="Code ussd" name="ussd" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="feedback4" class="sr-only">Mot cles</label>
                                                            <input type="text" id="feedback4" class="form-control" placeholder="Mot cles" name="key" required>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button  type="submit" class="btn btn-primary" >Valider</button>
                                                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                                </div>
                                            </form>
                                    </div>
                                </div>
                    </div>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#primary">
                Ajouter un operateur
            </button>
            <p class="card-text">
                La liste des operateurs
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Reseau</th>
                        <th>Ussd</th>
                        <th>Mot cles</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($operateur as $reseau)
                        <tr>
                            <td>{{$reseau['operateurs']}}</td>
                            <td>{{$reseau["ussd"]}}</td>
                            <td>{{$reseau["key"]}}</td>
                            <td><button class="btn btn-primary updateussd" data-toggle="modal"  data-id={{$reseau["id"]}}  data-target="#updateussd" >Modifier ussd</button>
                                <button class="btn btn-primary updatekey" data-toggle="modal"  data-id={{$reseau["id"]}}  data-target="#updatekey" >Modifier Mot cles</button>
                                <button class="btn btn-danger crediter" data-toggle="modal"  data-id={{$reseau->id}}  data-target="#Crediter" >Supprimer</button>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>


</section>

@endsection
<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="updateussd" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modifier code ussd</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="{{ route('updateussd') }}">
                                        @csrf
                                        <div class="modal-body">

                                            <div class="form-body">
                                                <input type="hidden" class="form-control" name="idoperateur" id="idk1" value="">
                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Code ussd</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Code ussd" name="ussd" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button  type="submit" class="btn btn-primary" >Valider</button>
                                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
            </div>
</div>

<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="updatekey" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modifier Mot cles</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="{{ route('updatekey') }}">
                                        @csrf
                                        <div class="modal-body">

                                            <div class="form-body">
                                                <input type="hidden" class="form-control" name="idoperateur" id="idk1" value="">
                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Mot cles</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Mot cles" name="key" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button  type="submit" class="btn btn-primary" >Valider</button>
                                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
            </div>
</div>


@section('js_special')
<script type="text/javascript">
    /* Formating function for row details */
      $(document).on("click", ".updateussd", function () {
       var id = $(this).attr('data-id');
       $(".modal-body #idk1").val( id );
      });

      $(document).on("click", ".updatekey", function () {
       var id = $(this).attr('data-id');
       $(".modal-body #idk1").val( id );
      });
  </script>


@endsection



