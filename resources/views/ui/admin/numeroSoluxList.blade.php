@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste de nos Numeros Mobile Money
        </div>
        <div class="col-12 col-md-12 ml-3 mr-5">
            <div class="modal fade" id="primary" tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <h5 class="modal-title white" id="myModalLabel160">Ajouter un numero</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i data-feather="x"></i>
                                                </button>
                                            </div>
                                            <form class="form" method="post" action="{{route('numeroSoluxadd')}}">
                                                @csrf
                                                <div class="modal-body">
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label for="feedback1" class="sr-only">Operateur</label>
                                                                <select class="form-control" name="moyen" >
                                                                    <option value="" disabled>--Choix de l'operaterateur--</option>
                                                                    @foreach ($moyen as $moyen)
                                                                      <option value={{$moyen->id}}>{{$moyen->operateurs}}</option>

                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="feedback4" class="sr-only">Numero </label>
                                                                <input type="text" id="feedback4" class="form-control" placeholder="Numero" name="mobilemoney" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="feedback4" class="sr-only">Code Secret</label>
                                                                <input type="text" id="feedback4" class="form-control" placeholder="Code secret" name="pin" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="feedback4" class="sr-only">Somme </label>
                                                                <input type="number" id="feedback4" class="form-control" placeholder="Somme sur le numero" name="somme" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="feedback1" class="sr-only">Statut numero</label>
                                                                <select class="form-control" name="type" >
                                                                    <option value="" disabled>--Type--</option>
                                                                      <option value="E">Envoie</option>
                                                                </select>
                                                            </div>

                                                        </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button  type="submit" class="btn btn-primary" >Valider</button>
                                                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                                </div>
                                            </form>
                                    </div>
                                </div>
                    </div>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#primary">
                Ajouter un numero
            </button>
            <p class="card-text">
                La liste des numeros
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Reseau</th>
                        <th>Mobilemoney</th>
                        <th>Pin</th>
                        <th>Somme</th>
                        <th>Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($numero as $numero)
                        <tr>
                            <?php
                                $reseau = DB::table('moyen_payements')
                                                ->where('id',$numero["moyen_id"])
                                                ->first();
                            ?>
                            <td>{{$reseau->operateurs}}</td>
                            <td>{{$numero["mobilemoney"]}}</td>
                            <td>{{$numero["pin"]}}</td>
                            <td>{{$numero["somme"]}}</td>
                            <td>{{$numero["type"]}}</td>
                            <td><button class="btn btn-primary updatesomme" data-toggle="modal"  data-id={{$numero["id"]}}  data-target="#updatesomme" >Modifier somme</button>
                                <button class="btn btn-primary updatepin" data-toggle="modal"  data-id={{$numero["id"]}}  data-target="#updatepin" >Modifier pin</button>
                                <button class="btn btn-danger crediter" data-toggle="modal"  data-id={{$reseau->id}}  data-target="#Crediter" >Supprimer</button></td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="updatesomme" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modifier la somme</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="{{ route('updatesommenumero') }}">
                                        @csrf
                                        <div class="modal-body">

                                            <div class="form-body">
                                                <input type="hidden" class="form-control" name="idmobile" id="idk1" value="">
                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Somme</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Somme" name="somme" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button  type="submit" class="btn btn-primary" >Valider</button>
                                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
            </div>
</div>

<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="updatepin" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modifier code secret</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="{{ route('updatepin') }}">
                                        @csrf
                                        <div class="modal-body">

                                            <div class="form-body">
                                                <input type="hidden" class="form-control" name="idmobile" id="idk1" value="">
                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Code Secret</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Code secret" name="pin" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button  type="submit" class="btn btn-primary" >Valider</button>
                                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
            </div>
</div>
@section('js_special')
<script type="text/javascript">
    /* Formating function for row details */
      $(document).on("click", ".updatesomme", function () {
       var id = $(this).attr('data-id');
       $(".modal-body #idk1").val( id );
      });

      $(document).on("click", ".updatepin", function () {
       var id = $(this).attr('data-id');
       $(".modal-body #idk1").val( id );
      });
  </script>


@endsection
