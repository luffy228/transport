@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
    <section id="input-style">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center">Operateurs</h4>
                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="roundText">Libelle de l'Operateur</label>
                                    <input type="text" id="roundText" class="form-control round"
                                        placeholder="Rounded Input">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="roundText">code USSD</label>
                                    <input type="text" id="roundText" class="form-control round"
                                        placeholder="Rounded Input">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="roundText">Mot cle de retour ussd</label>
                                    <input type="text" id="roundText" class="form-control round"
                                        placeholder="Rounded Input">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">

                                    <button class="btn btn-success mt-4">Enregistrer l' operateur </button>
                                </div>

                            </div>

                        </div>
                        <div class="table-responsive">
                            <table class="table mb-0" id="table1">
                                <thead>
                                    <tr>
                                        <th>Libelle</th>
                                        <th>code</th>
                                        <th>Mot cle</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Graiden</td>
                                        <td>vehicula.aliquet@semconsequat.co.uk</td>
                                        <td>076 4820 8838</td>
                                        <td>

                                            <span class="badge bg-primary">Modifier</span>
                                            <span class="badge bg-secondary">Supprimer</span>



                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Dale</td>
                                        <td>fringilla.euismod.enim@quam.ca</td>
                                        <td>0500 527693</td>
                                        <td>New Quay</td>

                                    </tr>

                                    <tr>
                                        <td>Ganteng</td>
                                        <td>velit@nec.com</td>
                                        <td>0309 690 7871</td>
                                        <td>Ways</td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-header">
                        <h4 class="text-center">Channels</h4>
                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-sm-3 offset-2">
                                <div class="form-group">
                                    <label for="roundText">Nom du channel</label>
                                    <input type="text" id="roundText" class="form-control round"
                                        placeholder="Rounded Input">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="roundText">Operateur concernee</label>
                                    <input type="text" id="roundText" class="form-control round"
                                        placeholder="Rounded Input">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">

                                    <button class="btn btn-info mt-4">Creer le channel </button>
                                </div>

                            </div>

                        </div>
                        <div class="table-responsive">
                            <table class="table mb-0" id="table1">
                                <thead>
                                    <tr>
                                        <th>Libelle</th>
                                        <th>code</th>
                                        <th>Mot cle</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Graiden</td>
                                        <td>vehicula.aliquet@semconsequat.co.uk</td>
                                        <td>076 4820 8838</td>
                                        <td>

                                            <span class="badge bg-primary">Modifier</span>
                                            <span class="badge bg-secondary">Supprimer</span>



                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Dale</td>
                                        <td>fringilla.euismod.enim@quam.ca</td>
                                        <td>0500 527693</td>
                                        <td>New Quay</td>

                                    </tr>

                                    <tr>
                                        <td>Ganteng</td>
                                        <td>velit@nec.com</td>
                                        <td>0309 690 7871</td>
                                        <td>Ways</td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center">Numeros de transfert</h4>
                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="roundText">Numero de telephone</label>
                                    <input type="text" id="roundText" class="form-control round"
                                        placeholder="Rounded Input">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="roundText">Operateur</label>
                                    <fieldset class="form-group">
                                        <select class="form-select" id="basicSelect">
                                            <option>IT</option>
                                            <option>Blade Runner</option>
                                            <option>Thor Ragnarok</option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-sm-3">

                                    <label for="roundText">Channel de connexion</label>
                                    <fieldset class="form-group">
                                        <select class="form-select" id="basicSelect">
                                            <option>IT</option>
                                            <option>Blade Runner</option>
                                            <option>Thor Ragnarok</option>
                                        </select>
                                    </fieldset>

                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">

                                    <button class="btn btn-warning mt-4">Enregistrer le numero </button>
                                </div>

                            </div>

                        </div>
                        <div class="table-responsive">
                            <table class="table mb-0" id="table1">
                                <thead>
                                    <tr>
                                        <th>Libelle</th>
                                        <th>code</th>
                                        <th>Mot cle</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Graiden</td>
                                        <td>vehicula.aliquet@semconsequat.co.uk</td>
                                        <td>076 4820 8838</td>
                                        <td>

                                            <span class="badge bg-primary">Modifier</span>
                                            <span class="badge bg-secondary">Supprimer</span>



                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Dale</td>
                                        <td>fringilla.euismod.enim@quam.ca</td>
                                        <td>0500 527693</td>
                                        <td>New Quay</td>

                                    </tr>

                                    <tr>
                                        <td>Ganteng</td>
                                        <td>velit@nec.com</td>
                                        <td>0309 690 7871</td>
                                        <td>Ways</td>

                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

