@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')

    <div class="col-12 col-md-12 ml-3 mr-5">
        <div class="modal fade" id="primary" tabindex="-1" role="dialog"
                            aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                <h5 class="modal-title white" id="myModalLabel160">Ajouter une ligne</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i data-feather="x"></i>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <form class="form" method="post">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="feedback1" class="sr-only">Ville de depart</label>
                                                <input type="text" id="feedback1" class="form-control" placeholder="Ville de depart" name="depart">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback4" class="sr-only">Ville de destination</label>
                                                <input type="text" id="feedback4" class="form-control" placeholder="Ville de destination" name="destination">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback2" class="sr-only">Heure depart</label>
                                                <input type="time" id="feedback2" class="form-control" placeholder="Heure de depart"
                                                min="05:00" max="12:00"
                                                name="time">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback2" class="sr-only">Prix du ticket</label>
                                                <input type="email" id="feedback2" class="form-control" placeholder="Prix unitaire du ticket" name="price">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback2" class="sr-only">Duree moyenne du trajet</label>
                                                <input type="time" id="feedback2" class="form-control" placeholder="Duree du trajet" name="duree">
                                            </div>

                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                    <i class="bx bx-x d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Annuler</span>
                                </button>
                                <button type="button" class="btn btn-primary ml-1" data-dismiss="modal">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Enregistrer</span>
                                </button>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
        <div class="card">
            <div class="card-header">
                <h2 class="text-center">Informations des transactions de paiements</h2>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nom du client</th>
                                    <th>Ligne de selection</th>
                                    <th>Date de paiement</th>
                                    <th>Date du voyage </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-bold-500">Michael Right</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">Michael Right</td>
                                </tr>
                                <tr>
                                    <td class="text-bold-500">Morgan Vanblum</td>
                                    <td>$13/hr</td>
                                    <td class="text-bold-500">Graphic concepts</td>
                                    <td class="text-bold-500">Michael Right</td>
                                </tr>
                                <tr>
                                    <td class="text-bold-500">Tiffani Blogz</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">Animation</td>
                                    <td class="text-bold-500">Michael Right</td>
                                </tr>
                                <tr>
                                    <td class="text-bold-500">Ashley Boul</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">Animation</td>
                                    <td>$15/hr</td>
                                </tr>
                                <tr>
                                    <td class="text-bold-500">Mikkey Mice</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">Animation</td>
                                    <td class="text-bold-500">UI/UX</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
