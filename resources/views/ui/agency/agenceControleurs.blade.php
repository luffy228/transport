@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')

    <div class="col-12 col-md-12 ml-3 mr-5">
        <div class="modal fade" id="primary" tabindex="-1" role="dialog"
                            aria-labelledby="myModalLabel160" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-warning">
                                <h5 class="modal-title white" id="myModalLabel160">Ajouter controleur</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i data-feather="x"></i>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <form class="form" method="post">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="feedback1" class="sr-only">Nom</label>
                                                <input type="text" id="feedback1" class="form-control" placeholder="Nom du controlleur" name="name">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback4" class="sr-only">Prenom(s)</label>
                                                <input type="text" id="feedback4" class="form-control" placeholder="Prenom(s) du controlleur" name="destination">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback2" class="sr-only">Telephone</label>
                                                <input type="tel" id="feedback2" class="form-control" placeholder="Nuemro de telephone" name="telephone">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback2" class="sr-only">Mot de passe</label>
                                                <input type="password" id="feedback2" class="form-control" placeholder="Mot de passe(par defaut du compte)" name="password">
                                            </div>
                                            <div class="form-group">
                                                <label for="feedback2" class="sr-only">Confirmer le mot de passe</label>
                                                <input type="password" id="feedback2" class="form-control" placeholder="Confirmer le mot de passe" name="confirmPassword">
                                            </div>

                                        </div>

                                    </form>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                                    <i class="bx bx-x d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Annuler</span>
                                </button>
                                <button type="button" class="btn btn-warning ml-1" data-dismiss="modal">
                                    <i class="bx bx-check d-block d-sm-none"></i>
                                    <span class="d-none d-sm-block">Enregistrer</span>
                                </button>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Les controleurs de votre agence</h2>
                <p>
                    <button type="button" class="btn btn-outline-warning float-right" data-toggle="modal" data-target="#primary">
                        Ajouter un controleur
                    </button>

                </p>
            </div>
            <div class="card-content">
                <div class="card-body">

                    <!-- Table with outer spacing -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>Telephone</th>
                                    <th>Ligne affectee</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-bold-500">Michael Right</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">Michael Right</td>
                                    <td class="text-bold-500">Michael Right</td>
                                </tr>
                                <tr>
                                    <td class="text-bold-500">Morgan Vanblum</td>
                                    <td>$13/hr</td>
                                    <td class="text-bold-500">Graphic concepts</td>
                                    <td class="text-bold-500">Michael Right</td>
                                    <td class="text-bold-500">Michael Right</td>

                                </tr>
                                <tr>
                                    <td class="text-bold-500">Tiffani Blogz</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">Animation</td>
                                    <td class="text-bold-500">Michael Right</td>
                                    <td class="text-bold-500">Michael Right</td>
                                </tr>

                                <tr>
                                    <td class="text-bold-500">Mikkey Mice</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">Animation</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">Michael Right</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
