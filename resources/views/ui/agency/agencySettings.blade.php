@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h1 class="text-center">Votre agence</h1>
        </div>
        
        <div class="card-body">
            <span class="text-left mb-4"><h2>Informations globales</h2></span>
            <div class="row mb-4">
                <div class="form-group col-md-4">
                    <label for="disabledInput">Nom de votre agence</label>
                    <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="You can't update me :P">
                </div>
                <div class="form-group col-md-4">
                    <label for="disabledInput">Telephone</label>
                    <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="You can't update me :P">
                </div>
                <div class="form-group col-md-4">
                    <label for="disabledInput">Email</label>
                    <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="You can't update me :P">
                </div>
            </div>
            <span class="text-left "><h2>Informations pour payement</h2></span>
            <div class="row mt-2">
                <div class="form-group col-md-4">
                    <label for="disabledInput">Operateur pour paiement</label>
                    <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="You can't update me :P">
                </div>
                <div class="form-group col-md-4">
                    <label for="disabledInput">Numero de reception</label>
                    <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="You can't update me :P">
                </div>
            </div>
           
            <span class="text-left "><h2>Informations de connexion</h2></span>
            <div class="row mt-2">
                <div class="form-group col-md-4">
                    <label for="disabledInput">Operateur pour paiement</label>
                    <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="You can't update me :P">
                </div>
                <div class="form-group col-md-4">
                    <label for="disabledInput">Numero de reception</label>
                    <input type="text" class="form-control" id="readonlyInput" readonly="readonly" value="You can't update me :P">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection