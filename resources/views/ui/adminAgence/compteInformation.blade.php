@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
    <section id="input-style">
        <div class="row">
            <div class="col-md-12">

                @hasrole('AdministrateurAgence')
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-center">Information Agence</h4>
                        </div>

                        <div class="card-body">

                            <div class="row">

                                <div class="col-sm-3">

                                    <div class="mb-2">
                                        <p>Nom</p>

                                    </div>

                                    <div>
                                        <p>{{$agence->name}}</p>

                                    </div>

                                </div>
                                <div class="col-sm-3">

                                    <div class="mb-2">
                                        <p>Addresse</p>

                                    </div>

                                    <div>
                                        <p>{{$agence->addresse}}</p>

                                    </div>

                                </div>
                                <div class="col-sm-3">

                                    <div class="mb-2">
                                        <p>Telephone</p>

                                    </div>

                                    <div>
                                        <p>{{$agence->telephone}}</p>

                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#primary">
                                            Modifier
                                        </button>


                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                @endhasrole


                <div class="card mt-5">

                    <div class="card-header">
                        <h4 class="text-center">Information Utilisateur</h4>
                    </div>

                    <div class="card-body">

                        <div class="row">

                            <div class="col-sm-2">

                                <div class="mb-1">
                                    <p>Role</p>

                                </div>

                                <div>
                                    <p>{{$role}}</p>

                                </div>

                            </div>
                            <div class="col-sm-2">

                                <div class="mb-2">
                                    <p>Nom Utilisateur</p>

                                </div>

                                <div>
                                    <p>{{$user->name}}</p>

                                </div>

                            </div>
                            <div class="col-sm-2">

                                <div class="mb-2">
                                    <p>Email / Telephone</p>

                                </div>

                                <div>
                                    <p>{{$user->email}} / {{$user->telephone}}</p>

                                </div>

                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">

                                    <button type="button" class="btn btn-outline-success mt-4" data-toggle="modal" data-target="#updateuser">
                                        Modifier information
                                    </button>
                                    <button type="button" class="btn btn-outline-success mt-4 " data-toggle="modal" data-target="#updatepassword">
                                        Modifier password
                                    </button>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="primary" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modification information agence</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="{{route('updateAgence')}}">
                                        @csrf
                                    <div class="modal-body">
                                        <input type="hidden" id="feedback4" class="form-control" name="agence_id" value={{$agence->agence_id}}>

                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Nom agence</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Nom de l'agence" name="nomAgence">
                                                </div>

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Adresse agence</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Adresse de l'agence" name="adresseAgence">
                                                </div>

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Telephone agence</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Telephone de l'agence" name="telephoneAgence">
                                                </div>
                                            </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button  type="submit" class="btn btn-primary" >Valider</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                    </div>
                            </div>
                        </form>
                        </div>
            </div>
</div>

<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="updatepassword" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modification mot de passe</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="{{route('updatePassword')}}">
                                        @csrf
                                    <div class="modal-body">
                                        <input type="hidden" id="feedback4" class="form-control" name="user_id" value={{$user->id}}>

                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Ancien mot de passe</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Ancien mot de password" name="lastpassword">
                                                </div>

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Nouveau mot de passe</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Nouveau mot de passe" name="newpassword">
                                                </div>
                                            </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button  type="submit" class="btn btn-primary" >Valider</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                    </div>
                            </div>
                        </form>
                        </div>
            </div>
</div>

<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="updateuser" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modification information de l'utilisateur</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="{{route('updateUser')}}">
                                        @csrf
                                    <div class="modal-body">

                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Nom utilisateur</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Nom de l'utilisateur" name="nom">
                                                </div>

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">email utilisateur</label>
                                                    <input type="email" id="feedback4" class="form-control" placeholder="email de l'utilisateur" name="email">
                                                </div>

                                                <div class="form-group">
                                                    <label for="feedback4" class="sr-only">Telephone agence</label>
                                                    <input type="text" id="feedback4" class="form-control" placeholder="Telephone de l'agence" name="telephoneAgence">
                                                </div>
                                            </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button  type="submit" class="btn btn-primary" >Valider</button>
                                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                    </div>
                            </div>
                        </form>
                        </div>
            </div>
</div>

