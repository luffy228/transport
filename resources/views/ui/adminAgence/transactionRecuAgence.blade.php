@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Transaction Recu
        </div>
        <div class="card-body">
            <p class="card-text">
                Liste des transactions recu de la part de E-solux
            </p>
            <div class="row mb-2">
                <div class="col-12 col-md-3">
                    <div class="card card-statistic">
                        <div class="card-body p-0">
                            <div class="d-flex flex-column">
                                <div class='px-3 py-3 d-flex justify-content-between'>
                                    <h3 class='card-title'>Total Transactions</h3>
                                    <div class="card-right d-flex align-items-center">
                                        <p>{{$countTotal}} </p>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <canvas id="canvas1" style="height:100px !important"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="card card-statistic">
                        <div class="card-body p-0">
                            <div class="d-flex flex-column">
                                <div class='px-3 py-3 d-flex justify-content-between'>
                                    <h3 class='card-title'>Somme Recus</h3>
                                    <div class="card-right d-flex align-items-center">
                                        <p>{{$sommeTotal}}  Francs CFA </p>
                                    </div>
                                </div>
                                <div class="chart-wrapper">
                                    <canvas id="canvas2" style="height:100px !important"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Numero Destinataire</th>
                        <th>Information</th>

                    </tr>
                </thead>
                <tbody>
                        @foreach ($info as $transaction)
                            <tr>
                                <td>{{$transaction->created_at}}</td>
                                <td>{{$transaction->numeroAgence}}</td>
                                <td>{{$transaction->nombreTicket}} Billet/ {{$transaction->prix}} Francs CFA </td>
                            </tr>

                        @endforeach



                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
