@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Ajouter un voyage
        </div>
        <div class="card-body">
            <form class="form" method="POST" action="{{route('ligneadd')}}">
                @csrf

                <section id="multiple-column-form">
                    <div class="row match-height">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Information d'un voyage</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="first-name-column">Point de depart</label>
                                                        <select class="form-control" name="depart" >
                                                            <option value="" disabled>--Choix du point de depart--</option>
                                                            @foreach ($ville as $depart)
                                                            <option value={{$depart["nom"]}}>{{$depart["nom"]}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Point d'arriver</label>
                                                        <select class="form-control" name="arriver" >
                                                            <option value="" disabled>--Choix du point d'arriver--</option>
                                                            @foreach ($ville as $arriver)
                                                            <option value={{$arriver["nom"]}}>{{$arriver["nom"]}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="city-column">Date de depart</label>
                                                        <input type="date" id="city-column" class="form-control" placeholder="Date de depart" name="datedepart" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="email-id-column">Heure de depart</label>
                                                        <input type="time" id="feedback2" class="form-control" placeholder="Heure de depart"
                                                                min="00:00" max="23:59" name="timedepart" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Date d'arriver</label>
                                                        <input type="date" id="country-floating" class="form-control" name="datearriver"
                                                            placeholder="Date d'arriver" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Heure d'arriver</label>
                                                        <input type="time" id="feedback2" class="form-control" placeholder="Heure de depart"
                                                                min="00:00" max="23:59" name="timearriver" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Prix du billet</label>
                                                        <input type="number" id="country-floating" class="form-control" name="prix"
                                                            placeholder="Le prix du billet" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Total de place</label>
                                                        <input type="number" id="country-floating" class="form-control" name="totalplace"
                                                            placeholder="Nombre de place disponible" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Poids d'un bagage</label>
                                                        <input type="number" id="country-floating" class="form-control" name="poids"
                                                            placeholder="Poids d'un bagage" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                    <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>


        </div>
    </div>

</section>
@endsection
