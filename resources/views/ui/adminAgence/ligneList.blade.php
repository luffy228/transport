@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste des lignes de l'agence
        </div>

        <div class="card-body">
            <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#primary">
                <a href="{{route('formligne')}}">Ajouter une ligne</a>

            </button>
            <p class="card-text">
                La liste des lignes de l'agence
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Depart</th>
                        <th>Arriver</th>
                        <th>Duree</th>
                        <th>Prix</th>
                        <th>Total place</th>
                        <th>Poids par voyageur(kg)</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($ticket as $info )
                        <tr>
                            <td>{{$info->name}}</td>
                            <td>{{$info->dateDepart}} {{$info->heureDepart}}</td>
                            <td>{{$info->dateArriver}} {{$info->heureArriver}}</td>
                            <td>{{$info->duree}}</td>
                            <td>{{$info->prix}} Frcs </td>
                            <td>{{$info->totalPlace}}</td>
                            <td>{{$info->poids}}</td>
                            <td>Actions</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="update" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modifier le ticket</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="#">
                                        @csrf
                                        <div class="modal-body">

                                            <div class="form-body">
                                                <input type="hidden" class="form-control" name="idticket" id="idk1" value="">
                                                <div class="form-group">
                                                    <label for="feedback1" class="sr-only">Date depart</label>
                                                    <input type="date" id="feedback1" class="form-control" onfocus="Date depart" name="depart">
                                                </div>
                                                <div class="form-group">
                                                    <label for="feedback2" class="sr-only">Heure depart</label>
                                                    <input type="time" id="feedback2" class="form-control" placeholder="Heure de depart"
                                                    min="00:00" max="24:00"
                                                    name="time">
                                                </div>
                                                <div class="form-group">
                                                    <label for="feedback2" class="sr-only">Prix du ticket</label>
                                                    <input type="email" id="feedback2" class="form-control" placeholder="Prix unitaire du ticket" name="price">
                                                </div>

                                                <div class="form-group">
                                                    <label for="feedback2" class="sr-only">Total Place</label>
                                                    <input type="email" id="feedback2" class="form-control" placeholder="Prix unitaire du ticket" name="price">
                                                </div>

                                                <div class="form-group">
                                                    <label for="feedback2" class="sr-only">Poids bagage</label>
                                                    <input type="email" id="feedback2" class="form-control" placeholder="Prix unitaire du ticket" name="price">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button  type="submit" class="btn btn-primary" >Valider</button>
                                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
            </div>
</div>

<div class="col-12 col-md-12 ml-3 mr-5">
    <div class="modal fade" id="updateTime" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel160" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel160">Modifier la duree</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <form class="form" method="post" action="#">
                                        @csrf
                                        <div class="modal-body">

                                            <div class="form-body">
                                                <input type="hidden" class="form-control" name="idticket" id="idk1" value="">

                                                <div class="form-group">
                                                    <label for="feedback2" class="sr-only">duree</label>
                                                    <input type="time" id="feedback2" class="form-control" placeholder="Heure de depart"
                                                    min="00:00" max="24:00"
                                                    name="time">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button  type="submit" class="btn btn-primary" >Valider</button>
                                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                        </div>
                                    </form>
                            </div>
                        </div>
            </div>
</div>
@section('js_special')
<script type="text/javascript">
    /* Formating function for row details */
      $(document).on("click", ".update", function () {
       var id = $(this).attr('data-id');
       $(".modal-body #idk1").val( id );
      });

      $(document).on("click", ".updateTime", function () {
       var id = $(this).attr('data-id');
       $(".modal-body #idk1").val( id );
      });
  </script>
@endsection
