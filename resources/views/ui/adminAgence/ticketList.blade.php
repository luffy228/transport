@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Etat des tickets des lignes
        </div>

        <div class="card-body">
            <p class="card-text">
                Ticket en cours de ventes
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Nom de la ligne</th>
                        <th>Ticket vendu</th>
                        @hasrole('AdministrateurAgence')
                        <th>Somme recolter</th>
                        @endhasrole

                    </tr>
                </thead>
                <tbody>

                    @foreach ($ticket as $info )
                        <tr>
                            <td>{{$info->name}}</td>
                            <td>{{$info->totalPlace - $info->place }}</td>
                            @hasrole('AdministrateurAgence')
                            <td>{{$info->prix * ($info->totalPlace - $info->place)}} Francs CFA</td>
                            @endhasrole

                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
