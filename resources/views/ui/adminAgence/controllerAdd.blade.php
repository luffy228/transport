@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Ajouter un controller
        </div>
        <div class="card-body">
            <form class="form" method="POST" action="{{route('controllerAdd')}}">
                @csrf

                <section id="multiple-column-form">
                    <div class="row match-height">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Information controller</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="first-name-column">Noms et prenoms</label>
                                                        <input type="text" id="first-name-column" class="form-control" placeholder="nom et prenoms"
                                                            name="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="last-name-column">Nom d'utilisateur</label>
                                                        <input type="text" id="last-name-column" class="form-control" placeholder="noms d'utilisateur"
                                                            name="username" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="city-column">Mot de passe</label>
                                                        <input type="text" id="city-column" class="form-control" placeholder="mot de passe" name="password" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="email-id-column">Email</label>
                                                        <input type="email" id="email-id-column" class="form-control" name="email"
                                                            placeholder="Email" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label for="country-floating">Numero de telephone</label>
                                                        <input type="text" id="country-floating" class="form-control" name="telephone"
                                                            placeholder="Numero de telephone du gerant" required>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                    <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>


        </div>
    </div>

</section>
@endsection
