@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Liste des agents
        </div>

        <div class="card-body">
            <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#primary">
                <a href="{{route('agentform')}}">Ajouter un agent</a>

            </button>
            <p class="card-text">
                La liste des agents
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Telephone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($responsable as $responsable)
                            <tr>
                                <td>{{$responsable->name}}</td>
                                <td>{{$responsable->telephone}}</td>
                                <td>Actions </td>
                            </tr>

                        @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
