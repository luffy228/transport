@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')
<section class="section">
    <div class="card">
        <div class="card-header">
            Parametre Mobile Money
        </div>
        <div class="col-12 col-md-12 ml-3 mr-5">
            <div class="modal fade" id="primary" tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel160" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                            <div class="modal-header bg-primary">
                                                <h5 class="modal-title white" id="myModalLabel160">Ajouter un numero Mobile Money</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i data-feather="x"></i>
                                                </button>
                                            </div>
                                            <form class="form" method="post" action="{{route('moneyadd')}}">
                                                @csrf
                                            <div class="modal-body">
                                                <input type="hidden" id="feedback4" class="form-control" name="agence_id" value={{$agence->agence_id}}>

                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label for="feedback1" class="sr-only">Operateurs</label>
                                                            <select class="form-control" name="moyen" >
                                                                <option value="" disabled>--Choix de l'operaterateur--</option>
                                                                @foreach ($moyen as $moyen)
                                                                <option value={{$moyen->id}}>{{$moyen->operateurs}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="feedback4" class="sr-only">Numero Mobile Money</label>
                                                            <input type="text" id="feedback4" class="form-control" placeholder="Numero Mobile Money" name="mobilemoney">
                                                        </div>
                                                    </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button  type="submit" class="btn btn-primary" >Valider</button>
                                                <button type="reset" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                                            </div>
                                    </div>
                                </form>
                                </div>
                    </div>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#primary">
                Ajouter un numero Mobile Money
            </button>
            <p class="card-text">
                La liste des numeros mobile money
            </p>
            <table class='table table-striped' id="table1">
                <thead>
                    <tr>
                        <th>Operateur</th>
                        <th>Numero</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($info as $mobile )
                        <tr>
                            <?php $reseau = DB::table('moyen_payements')
                                                        ->where('id',$mobile->moyen_id)
                                                        ->first() ?>
                            <td>{{$reseau->operateurs}}</td>
                            <td>{{$mobile->mobilemoney}}</td>
                            <td>Actions </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</section>
@endsection
