<div class="main-content container-fluid">
    <div class="page-title">
        <h3>Panneau d'administration</h3>
        <p class="text-subtitle text-muted"></p>
    </div>
    <section class="section">
        <div class="row mb-2">
            <div class="col-12 col-md-3">
                <div class="card card-statistic">
                    <div class="card-body p-0">
                        <div class="d-flex flex-column">
                            <div class='px-3 py-3 d-flex justify-content-between'>
                                <h3 class='card-title'>Nombre de ligne</h3>
                                <div class="card-right d-flex align-items-center">
                                    <p>{{$data["ligne"]}} </p>
                                </div>
                            </div>
                            <div class="chart-wrapper">
                                <canvas id="canvas1" style="height:100px !important"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        </div>
    </section>
</div>
