@extends('layouts.app')

@section('content')
<div id="app">
 @include('../layouts/partials/_nav')
    <div id="main">
        @include('../layouts/partials/_header')
        @include('sweetalert::alert')


        @yield('contentAdministrative')


@include('../layouts/partials/_footer')

    </div>
</div>
@endsection
