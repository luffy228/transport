@extends('ui.genericPages.adminstrative')

@section('contentAdministrative')

@hasrole('SuperAdministrateur')
            @include('../ui/admin/home')
@endhasrole

@hasrole('AdministrateurAgence')
            @include('../ui/adminAgence/home')
@endhasrole

@hasrole('Agents')
            @include('../ui/agents/home')
@endhasrole

@endsection
