<div id="sidebar" class='active'>
    <div class="sidebar-wrapper active">
<div class="sidebar-header">
<img src="{{asset('assets/images/billet.jpeg')}}" width="150"  alt="" srcset="">
</div>
<div class="sidebar-menu">
<ul class="menu">

    @auth

    @hasrole('SuperAdministrateur')
            @include('../ui/admin/navbar')
    @endhasrole

    @hasrole('AdministrateurAgence')
            @include('../ui/adminAgence/navbar')
    @endhasrole

    @hasrole('Agents')
            @include('../ui/agents/navbar')
    @endhasrole

    @endauth

    @guest


    @endguest




</ul>
</div>
<button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
</div>
</div>
