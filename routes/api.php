<?php

use App\Http\Controllers\AgenceController;
use App\Http\Controllers\AgentController;
use App\Http\Controllers\Api\AgenceController as ApiAgenceController;
use App\Http\Controllers\Api\DestinataireInfoController;
use App\Http\Controllers\Api\OperateurController;
use App\Http\Controllers\Api\StatistiqueSoluxController;
use App\Http\Controllers\Api\TicketController as ApiTicketController;
use App\Http\Controllers\Api\TransactionSoluxController;
use App\Http\Controllers\Api\TransactionUserController as ApiTransactionUserController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\VendeurController as ApiVendeurController;
use App\Http\Controllers\Api\VerifieurController;
use App\Http\Controllers\LigneController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TransactionUserController;
use App\Http\Controllers\VendeurController;
use App\Http\Controllers\VilleController;
use App\Models\Transaction;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//login
Route::prefix('/v1')->group(function () {
    Route::post('/login', [UserController::class, 'login']);
    Route::post('/register', [UserController::class, 'register']);
    Route::get('/ville', [UserController::class, 'getVille']);
    Route::post('/logout', [UserController::class, 'logout']);
    Route::post('/ticket/find', [ApiTicketController::class, 'find']);
    Route::post('/forgetPassword', [UserController::class, 'forgetPassword']);

});
Route::prefix('/v1/vendeur')->group(function () {
    Route::post('/login', [ApiVendeurController::class, 'login']);
});

Route::prefix('/v1/controlleur')->group(function () {
    Route::post('/login', [VerifieurController::class, 'login']);
});
//

Route::middleware(['auth:api','role:Voyageur|Vendeur'])->prefix("/v1")->group(function () {
    Route::get('/user/reservation', [UserController::class, 'ListeReservation']);
    Route::post('/ticket/findAgence', [ApiTicketController::class, 'findAgence']);
    Route::post('/transactionUser/add', [ApiTransactionUserController::class, 'add']);
    Route::get('/operateur/liste', [OperateurController::class, 'liste']);
    Route::post('/operateur/findMobileMoney', [OperateurController::class, 'findMobileMoney']);
    Route::post('/destinataire/add', [DestinataireInfoController::class, 'add']);
     Route::post('/updatePassword', [UserController::class, 'updatePassword']);
});

Route::middleware(['auth:api','role:Vendeur'])->prefix("/v1/vendeur")->group(function () {
    Route::get('/ticketSales', [ApiVendeurController::class, 'salesToday']);
    Route::post('/historyticketSales', [ApiVendeurController::class, 'historySales']);
});

Route::middleware(['auth:api','role:Controleur'])->prefix("/v1/controlleur")->group(function () {
    Route::post('/countverifyticket', [VerifieurController::class, 'verifyticketToday']);
    Route::post('/historyverifyticket', [VerifieurController::class, 'Historyverifyticket']);
    Route::post('/findReservation', [ApiTransactionUserController::class, 'findReservation']);
    Route::post('/ValiderReservation', [ApiTransactionUserController::class, 'validerReservation']);
});

Route::prefix('/v1/transactionSolux')->group(function () {
    Route::post('/add', [TransactionSoluxController::class, 'add']);
});

Route::prefix('/v1/agence')->group(function () {
    Route::get('/liste', [ApiAgenceController::class, 'liste']);
    Route::post('/find', [ApiAgenceController::class, 'findAgence']);
    Route::post('/benefice', [ApiAgenceController::class, 'beneficeToday']);
    Route::post('/historybenefice', [ApiAgenceController::class, 'beneficeHistory']);
});

Route::prefix('/v1/solux')->group(function () {
    Route::get('/statistique', [StatistiqueSoluxController::class, 'statistiqueToday']);
    Route::post('/historiqueStats', [StatistiqueSoluxController::class, 'statistiqueHistory']);
});

