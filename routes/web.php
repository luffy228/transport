<?php

use App\Http\Controllers\AgenceController;
use App\Http\Controllers\AgentController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\VendeurController;
use App\Http\Controllers\web\AgenceController as WebAgenceController;
use App\Http\Controllers\web\AgenceGainController;
use App\Http\Controllers\web\AgenceMobileMoneyController;
use App\Http\Controllers\web\AgentController as WebAgentController;
use App\Http\Controllers\web\CompteController;
use App\Http\Controllers\web\FraisController;
use App\Http\Controllers\web\GeneriqueController;
use App\Http\Controllers\web\LigneController;
use App\Http\Controllers\web\NumeroSoluxController;
use App\Http\Controllers\web\OperateurControlleur;
use App\Http\Controllers\web\TransactionSoluxController;
use App\Http\Controllers\web\TransactionUserController;
use App\Http\Controllers\web\VendeurController as WebVendeurController;
use App\Http\Controllers\web\VilleController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes(['register' => false]);
Route::post('/login', 'HomeController@login')->name('SubmitLogin');
Route::get('/logout', 'HomeController@logout')->name('Logout');

Route::get('/',function()
{
    return view("auth.login");
})->name('loginPage');



Route::get('/home',[GeneriqueController::class,'index'])->name("accueil");

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/agence")->group(function () {
    Route::get('/all',[WebAgenceController::class,'index'])->name("agenceall");
    Route::get('/formulaire',[WebAgenceController::class,'formulaire'])->name("agenceform");
    Route::post('/add',[WebAgenceController::class,'store'])->name("agenceadd");
});

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/vendeur")->group(function () {
    Route::get('/all',[WebVendeurController::class,'index'])->name("vendeurall");
    Route::get('/formulaire',[WebVendeurController::class,'formulaire'])->name("vendeurform");
    Route::post('/add',[WebVendeurController::class,'store'])->name("vendeuradd");
});

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/ville")->group(function () {
    Route::get('/all',[VilleController::class,'index'])->name("villeall");
    Route::post('/add',[VilleController::class,'store'])->name("villeadd");
});

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/numeroSolux")->group(function () {
    Route::get('/all',[NumeroSoluxController::class,'index'])->name("numeroSoluxall");
    Route::post('/add',[NumeroSoluxController::class,'store'])->name("numeroSoluxadd");
    Route::post('/updatesomme',[NumeroSoluxController::class,'updatesomme'])->name("updatesommenumero");
    Route::post('/updatepin',[NumeroSoluxController::class,'updatepin'])->name("updatepin");
});

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/frais")->group(function () {
    Route::get('/all',[FraisController::class,'index'])->name("fraisall");
});

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/gain")->group(function () {
    Route::get('/agence',[AgenceGainController::class,'index'])->name("gainagence");
});

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/transaction")->group(function () {
    Route::get('/solux',[TransactionSoluxController::class,'index'])->name("transactionsoluxToday");
    Route::get('/user',[TransactionUserController::class,'index'])->name("transactionuserToday");
});

Route::middleware(['role:SuperAdministrateur'])->prefix("/solux/operateur")->group(function () {
    Route::get('/all',[OperateurControlleur::class,'index'])->name("operateurall");
    Route::post('/add',[OperateurControlleur::class,'store'])->name("operateuradd");
    Route::post('/updateussd',[OperateurControlleur::class,'updateussd'])->name("updateussd");
    Route::post('/updatekey',[OperateurControlleur::class,'updatekey'])->name("updatekey");
});

Route::middleware(['role:AdministrateurAgence'])->prefix("/adminagence")->group(function () {
    Route::get('/controlleur',[WebAgentController::class,'indexController'])->name("controllerall");
    Route::get('/formulaireController',[WebAgentController::class,'formulaireController'])->name("controllerform");
    Route::get('/formulaireAgent',[WebAgentController::class,'formulaireAgent'])->name("agentform");
    Route::post('/controlleradd',[WebAgentController::class,'storeController'])->name("controllerAdd");
    Route::post('/agentadd',[WebAgentController::class,'storeAgent'])->name("agentAdd");
    Route::post('/updateAgence',[CompteController::class,'updateAgence'])->name("updateAgence");
    Route::get('/agents',[WebAgentController::class,'indexAgent'])->name("agentall");
    Route::get('/parametrage',[AgenceMobileMoneyController::class,'listeMobileNumber'])->name("parametragemoneyall");
    Route::post('/agenceMobileNumber',[AgenceMobileMoneyController::class,'MobileNumberadd'])->name("moneyadd");
    Route::get('/transaction',[TransactionSoluxController::class,'agenceTransaction'])->name("transactionsagenceToday");
});

Route::middleware(['role:AdministrateurAgence|Agents'])->prefix("/adminagence/billet")->group(function () {
    Route::get('/ligne',[LigneController::class,'index'])->name("ligneall");
    Route::get('/formulaireligne',[LigneController::class,'formligne'])->name("formligne");
    Route::post('/ligneadd',[LigneController::class,'store'])->name("ligneadd");
    Route::get('/ticket',[LigneController::class,'ticket'])->name("ticketall");
    Route::post('/updatePassword',[CompteController::class,'updatePassword'])->name("updatePassword");
    Route::post('/updateUser',[CompteController::class,'updateUser'])->name("updateUser");
    Route::get('/information',[CompteController::class,'index'])->name("compteinfo");
});
