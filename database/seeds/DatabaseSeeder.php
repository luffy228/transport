<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(
            [
                PermissionSeeder::class,
                RoleSeeder::class,
                RolePermissionSeeder::class,
                ClientSeeder::class,
                AgenceSeeder::class,
                UserSeeder::class,
                LigneSeeder::class,
                MoyenSeeder::class,
                TicketSeeder::class,

            ]
        );

    }
}
