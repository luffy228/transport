<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insertOrIgnore([
            ['name' => 'SuperAdministrateur','guard_name'=>'web'],
            ['name' => 'AdministrateurAgence','guard_name'=>'web'],
            ['name' => 'Controleur','guard_name'=>'web'],
            ['name' => 'Voyageur','guard_name'=>'web'],
            ['name' => 'Vendeur','guard_name'=>'web'],
            ['name' => 'Agents','guard_name'=>'web'],
        ]);

    }
}
