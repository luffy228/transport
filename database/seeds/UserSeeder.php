<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::create([
            'name'=>'admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'telephone' => '90099009',
            'password'=>bcrypt('administrateur')
        ]);
        $role1 = Role::find(1);
        $admin->assignRole($role1);
    }
}
