<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('agents')) {
            Schema::table('agents',function(Blueprint $table){
                 if (!Schema::hasColumn('agents', 'agence_id')) {
                    $table->unsignedBigInteger('agence_id');
                    $table->foreign('agence_id')->references('id')->on('agences');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('agents')) {
            Schema::create('agents', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('agence_id');
                $table->foreign('agence_id')->references('id')->on('agences');
                $table->softDeletes();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
