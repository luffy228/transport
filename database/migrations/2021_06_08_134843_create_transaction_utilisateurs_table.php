<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionUtilisateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('transaction_utilisateurs')) {
            Schema::table('transaction_utilisateurs',function(Blueprint $table){
                 if (!Schema::hasColumn('transaction_utilisateurs', 'user_id')) {
                    $table->unsignedBigInteger('user_id');
                    $table->foreign('user_id')->references('id')->on('users');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'agence_id')) {
                    $table->unsignedBigInteger('agence_id');
                    $table->foreign('agence_id')->references('id')->on('agences');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'ticket_id')) {
                    $table->unsignedBigInteger('ticket_id');
                    $table->foreign('ticket_id')->references('id')->on('tickets');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'num_user')) {
                    $table->string('num_user');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'num_destinataire')) {
                    $table->string('num_destinataire');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'num_reservation')) {
                    $table->string('num_reservation');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'moyen_id')) {
                    $table->unsignedBigInteger('moyen_id');
                    $table->foreign('moyen_id')->references('id')->on('moyen_payements');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'nombre_ticket')) {
                    $table->integer('nombre_ticket');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'prix')) {
                    $table->integer('prix');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'gainSolux')) {
                    $table->double('gainSolux');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'ticket_statut')) {
                    $table->string('ticket_statut');
                 }
                 if (!Schema::hasColumn('transaction_utilisateurs', 'sms')) {
                    $table->string('sms');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('transaction_utilisateurs')) {
            Schema::create('transaction_utilisateurs', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
                $table->unsignedBigInteger('agence_id');
                $table->foreign('agence_id')->references('id')->on('agences');
                $table->unsignedBigInteger('ticket_id');
                $table->foreign('ticket_id')->references('id')->on('tickets');
                $table->string('num_user');
                $table->string('num_destinataire');
                $table->string('num_reservation');
                $table->unsignedBigInteger('moyen_id');
                $table->foreign('moyen_id')->references('id')->on('moyen_payements');
                $table->integer('nombre_ticket');
                $table->integer('prix');
                $table->double('gainSolux');
                $table->string('ticket_statut');
                $table->string('sms');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_utilisateurs');
    }
}
