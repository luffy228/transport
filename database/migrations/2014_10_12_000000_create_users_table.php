<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('users')) {
            Schema::table('users',function(Blueprint $table){
                 if (!Schema::hasColumn('users', 'name')) {
                    $table->string('name')->unique();
                 }
                 if (!Schema::hasColumn('users', 'username')) {
                    $table->string('username');
                 }
                 if (!Schema::hasColumn('users', 'password')) {
                    $table->string('password');
                 }
                 if (!Schema::hasColumn('users', 'email')) {
                    $table->string('email')->unique();
                 }
                 if (!Schema::hasColumn('users', 'telephone')) {
                    $table->string('telephone')->unique();
                 }
                 $table->nullableMorphs('user');
                 $table->softDeletes();
                 $table->rememberToken();
                 $table->timestamps();

            });
        }

        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique();
                $table->string('username');
                $table->string('password');
                $table->string('email')->unique();
                $table->string('telephone')->unique();
                $table->nullableMorphs('user');
                $table->softDeletes();
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
