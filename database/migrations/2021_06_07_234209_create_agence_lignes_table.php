<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenceLignesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('agence_lignes')) {
            Schema::table('agence_lignes',function(Blueprint $table){
                 if (!Schema::hasColumn('agence_lignes', 'agence_id')) {
                    $table->unsignedBigInteger('agence_id');
                    $table->foreign('agence_id')->references('id')->on('agences');
                 }

                 if (!Schema::hasColumn('agence_lignes', 'ligne_id')) {
                    $table->unsignedBigInteger('ligne_id');
                    $table->foreign('ligne_id')->references('id')->on('lignes');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('agence_lignes')) {
            Schema::create('agence_lignes', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('agence_id');
                $table->foreign('agence_id')->references('id')->on('agences');
                $table->unsignedBigInteger('ligne_id');
                $table->foreign('ligne_id')->references('id')->on('lignes');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agence_lignes');
    }
}
