<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenceMobilemoneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('agence_mobilemoneys')) {
            Schema::table('agence_mobilemoneys',function(Blueprint $table){
                 if (!Schema::hasColumn('agence_mobilemoneys', 'agence_id')) {
                    $table->unsignedBigInteger('agence_id');
                    $table->foreign('agence_id')->references('id')->on('agences');
                 }
                 if (!Schema::hasColumn('agence_mobilemoneys', 'mobilemoney')) {
                    $table->string('mobilemoney');
                }
                if (!Schema::hasColumn('agence_mobilemoneys', 'moyen_id')) {
                    $table->unsignedBigInteger('moyen_id');
                    $table->foreign('moyen_id')->references('id')->on('moyen_payements');
                }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('agence_mobilemoneys')) {
            Schema::create('agence_mobilemoneys', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('agence_id');
                $table->foreign('agence_id')->references('id')->on('agences');
                $table->string('mobilemoney');
                $table->unsignedBigInteger('moyen_id');
                $table->foreign('moyen_id')->references('id')->on('moyen_payements');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agence_mobilemoneys');
    }
}
