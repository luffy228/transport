<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobilesoluxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('mobilesoluxes')) {
            Schema::table('mobilesoluxes',function(Blueprint $table){
                 if (!Schema::hasColumn('mobilesoluxes', 'mobilemoney')) {
                    $table->string('mobilemoney');
                 }
                 if (!Schema::hasColumn('mobilesoluxes', 'somme')) {
                    $table->integer('somme');
                 }
                 if (!Schema::hasColumn('mobilesoluxes', 'pin')) {
                    $table->string('pin');
                 }
                 if (!Schema::hasColumn('mobilesoluxes', 'moyen_id')) {
                    $table->unsignedBigInteger('moyen_id');
                    $table->foreign('moyen_id')->references('id')->on('moyen_payements');
                 }
                 if (!Schema::hasColumn('mobilesoluxes', 'type')) {
                    $table->string('type');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('mobilesoluxes')) {
            Schema::create('mobilesoluxes', function (Blueprint $table) {
                $table->id();
                $table->string('mobilemoney');
                $table->integer('somme');
                $table->string('pin');
                $table->unsignedBigInteger('moyen_id');
                $table->foreign('moyen_id')->references('id')->on('moyen_payements');
                $table->string('type');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobilesoluxes');
    }
}
