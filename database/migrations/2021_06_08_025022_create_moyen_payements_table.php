<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoyenPayementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('moyen_payements')) {
            Schema::table('moyen_payements',function(Blueprint $table){
                 if (!Schema::hasColumn('moyen_payements', 'operateurs')) {
                    $table->string('operateurs');
                 }
                 if (!Schema::hasColumn('moyen_payements', 'key')) {
                    $table->string('key');
                 }
                 if (!Schema::hasColumn('moyen_payements', 'ussd')) {
                    $table->string('ussd');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('moyen_payements')) {
            Schema::create('moyen_payements', function (Blueprint $table) {
                $table->id();
                $table->string('operateurs');
                $table->string('ussd');
                $table->string('key');
                $table->softDeletes();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moyen_payements');
    }
}
