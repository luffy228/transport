<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenceFraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('agence_frais')) {
            Schema::table('agence_frais',function(Blueprint $table){
                 if (!Schema::hasColumn('agence_frais', 'agence_id')) {
                    $table->unsignedBigInteger('agence_id');
                    $table->foreign('agence_id')->references('id')->on('agences');
                 }
                 if (!Schema::hasColumn('agence_frais', 'pourcentage')) {
                    $table->integer('pourcentage');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('agence_frais')) {
            Schema::create('agence_frais', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('agence_id');
                $table->foreign('agence_id')->references('id')->on('agences');
                $table->integer('pourcentage');
                $table->softDeletes();
                $table->timestamps();
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agence_frais');
    }
}
