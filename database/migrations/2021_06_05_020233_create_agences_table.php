<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('agences')) {
            Schema::table('agences',function(Blueprint $table){
                 if (!Schema::hasColumn('agences', 'name')) {
                    $table->string('name')->unique();
                 }
                 if (!Schema::hasColumn('agences', 'addresse')) {
                    $table->string('addresse');
                 }

                 if (!Schema::hasColumn('agences', 'telephone')) {
                    $table->string('telephone')->unique();
                 }
                 $table->softDeletes();
                 $table->timestamps();

            });
        }

        if (!Schema::hasTable('agences')) {
            Schema::create('agences', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique();
                $table->string('addresse');
                $table->string('telephone')->unique();
                $table->softDeletes();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agences');
    }
}
