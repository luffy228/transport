<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionSoluxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('transaction_soluxes')) {
            Schema::table('transaction_soluxes',function(Blueprint $table){
                 if (!Schema::hasColumn('transaction_soluxes', 'agence_id')) {
                    $table->unsignedBigInteger('agence_id');
                    $table->foreign('agence_id')->references('id')->on('agences');
                 }

                 if (!Schema::hasColumn('transaction_soluxes', 'nombreTicket')) {
                    $table->integer('nombreTicket');
                 }
                 if (!Schema::hasColumn('transaction_soluxes', 'prix')) {
                    $table->integer('prix');
                 }
                 if (!Schema::hasColumn('transaction_soluxes', 'numeroAgence')) {
                    $table->string('numeroAgence');
                 }
                 if (!Schema::hasColumn('transaction_soluxes', 'statut')) {
                    $table->string('statut');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('transaction_soluxes')) {
            Schema::create('transaction_soluxes', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('agence_id');
                $table->foreign('agence_id')->references('id')->on('agences');
                $table->integer('nombreTicket');
                $table->integer('prix');
                $table->string('numeroAgence');
                $table->string('statut');
                $table->softDeletes();
                $table->timestamps();
            });
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_soluxes');
    }
}
