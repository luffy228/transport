<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLignesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('lignes')) {
            Schema::table('lignes',function(Blueprint $table){
                 if (!Schema::hasColumn('lignes', 'ville_depart_id')) {
                    $table->unsignedBigInteger('ville_depart_id');
                    $table->foreign('ville_depart_id')->references('id')->on('villes');
                 }

                 if (!Schema::hasColumn('lignes', 'ville_arriver_id')) {
                    $table->unsignedBigInteger('ville_arriver_id');
                    $table->foreign('ville_arriver_id')->references('id')->on('villes');
                 }

                 if (!Schema::hasColumn('lignes', 'name')) {
                    $table->string('name');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('lignes')) {
            Schema::create('lignes', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('ville_depart_id');
                $table->foreign('ville_depart_id')->references('id')->on('villes');
                $table->unsignedBigInteger('ville_arriver_id');
                $table->foreign('ville_arriver_id')->references('id')->on('villes');
                $table->string('name');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lignes');
    }
}
