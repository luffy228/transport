<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDestinataireInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('destinataire_infos')) {
            Schema::table('destinataire_infos',function(Blueprint $table){
                 if (!Schema::hasColumn('destinataire_infos', 'num_destinataire')) {
                    $table->string('num_destinataire')->unique();
                 }
                 if (!Schema::hasColumn('destinataire_infos', 'name')) {
                    $table->string('name');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('destinataire_infos')) {
            Schema::create('destinataire_infos', function (Blueprint $table) {
                $table->id();
                $table->string('num_destinataire')->unique();
                $table->string('name');
                $table->softDeletes();
                $table->timestamps();
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinataire_infos');
    }
}
