<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendeursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('vendeurs')) {
            Schema::table('vendeurs',function(Blueprint $table){
                 if (!Schema::hasColumn('vendeurs', 'carteIdentite')) {
                    $table->string('carteIdentite');
                 }
                 if (!Schema::hasColumn('vendeurs', 'cfe')) {
                    $table->string('cfe');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('vendeurs')) {
            Schema::create('vendeurs', function (Blueprint $table) {
                $table->id();
                $table->string('carteIdentite');
                $table->string('cfe');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendeurs');
    }
}
