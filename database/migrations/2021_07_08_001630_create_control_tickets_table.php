<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('control_tickets')) {
            Schema::table('control_tickets',function(Blueprint $table){
                 if (!Schema::hasColumn('control_tickets', 'user_id')) {
                    $table->unsignedBigInteger('user_id');
                    $table->foreign('user_id')->references('id')->on('users');
                 }
                 if (!Schema::hasColumn('control_tickets', 'transaction_id')) {
                    $table->unsignedBigInteger('transaction_id');
                    $table->foreign('transaction_id')->references('id')->on('transaction_utilisateurs');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('control_tickets')) {
            Schema::create('control_tickets', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users');
                $table->unsignedBigInteger('transaction_id');
                $table->foreign('transaction_id')->references('id')->on('transaction_utilisateurs');
                $table->softDeletes();
                $table->timestamps();
            });
        }

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_tickets');
    }
}
