<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tickets')) {
            Schema::table('tickets',function(Blueprint $table){
                 if (!Schema::hasColumn('tickets', 'agence_ligne_id')) {
                    $table->unsignedBigInteger('agence_ligne_id');
                    $table->foreign('agence_ligne_id')->references('id')->on('agence_lignes');
                 }
                 if (!Schema::hasColumn('tickets', 'heureDepart')) {
                    $table->time('heureDepart');
                 }
                 if (!Schema::hasColumn('tickets', 'heureArriver')) {
                    $table->time('heureArriver');
                 }
                 if (!Schema::hasColumn('tickets', 'duree')) {
                    $table->time('duree');
                 }
                 if (!Schema::hasColumn('tickets', 'dateDepart')) {
                    $table->date('dateDepart');
                 }
                 if (!Schema::hasColumn('tickets', 'dateArriver')) {
                    $table->date('dateArriver');
                 }
                 if (!Schema::hasColumn('tickets', 'prix')) {
                    $table->double('prix');
                 }
                 if (!Schema::hasColumn('tickets', 'frais')) {
                    $table->double('frais');
                 }
                 if (!Schema::hasColumn('tickets', 'totalPlace')) {
                    $table->integer('totalPlace');
                 }
                 if (!Schema::hasColumn('tickets', 'place')) {
                    $table->integer('place');
                 }
                 if (!Schema::hasColumn('tickets', 'poids')) {
                    $table->integer('poids');
                 }
                 $table->softDeletes();
                 $table->timestamps();
            });
        }

        if (!Schema::hasTable('tickets')) {
            Schema::create('tickets', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('agence_ligne_id');
                $table->foreign('agence_ligne_id')->references('id')->on('agence_lignes');
                $table->time('heureDepart');
                $table->time('heureArriver');
                $table->time('duree');
                $table->date('dateDepart');
                $table->date('dateArriver');
                $table->double('prix');
                $table->double('frais');
                $table->integer('totalPlace');
                $table->integer('place');
                $table->integer('poids');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
